package back.joueur;



import back.carte.Carte;
import back.carte.Couleur;
import back.exception.CoupInterditException;
import back.exception.TourException;
import back.exception.UnoException;
import back.partie.Partie;
import back.regles.RegleException;
import back.regles.Regles;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;


import java.util.ArrayList;
import java.util.Objects;

/**
 * Classe representant un joueur de la partie.
 *  Le joueur possede un plusieurs carte (jeu), il peut durant son tour de jeu effectue une action :
 * - poser une carte : si la coup est illegal, le joueur est puni et son tour finit.
 * - piocher une carte : le joueur pioche une carte du tas et peut la poser s'il le souhaite.
 * Si le joueur possede une carte et est le joueur courant il peut dire Uno, (il est puni dans le cas
 * ou il oublie de le dire).
 */
public class Joueur {
    //*********************************Champs*****************************************
    /**
     * Nom du joueur.
     */
    private String nom;
    /**
     * Le jeu du joueur.
     */
    private ArrayList<Carte> jeu = new ArrayList<Carte>();
    /**
     * Vrai quand le joueur dit uno legalement, faux sinon.
     */
    private boolean uno = false;
    /**
     * Vrai quand le joueur a effectue une action, faux sinon.
     */
    private boolean actionEffectue = false;
    /**
     * Vrai quand le joueur est le joueur courant faux sinon.
     */
    private boolean tourDeJeu = false;
    /**
     * Vrai si le joueur a pioche dans le tas de la partie durant son tour de jeu, faux sinon.
     */
    private boolean aPioche = false;
    /**
     * nombre de carte a pioche si le joueur est attaque.
     */
    private int carteAPiocher = 0;

    private Label label;


    //*********************************Constructeurs*****************************************

    /**
     * Constructeur d'un joueur.
     *
     * @param nom le nom d'un joueur.
     */
    public Joueur(String nom) {
        setNom(nom);
    }

    public Joueur(String nom, boolean tourDeJeu) {
        this.nom = nom;
        this.tourDeJeu = tourDeJeu;
    }

    //*********************************Setters*****************************************
    public void setTourDeJeu(boolean tourDeJeu) {
        this.tourDeJeu = tourDeJeu;
    }

    public void setNom(String nom) {
        if (nom == null || nom.trim().equals("")) {
            throw new IllegalArgumentException("Le nom du joueur est vide ou null");
        }
        this.nom = nom;
    }

    public void setActionEffectue(boolean action) {
        this.actionEffectue = action;
    }

    public void setCarteAPiocher(int carteAPiocher) {
        if(carteAPiocher<0){
            throw new IllegalArgumentException("nb de carte à piocher < 0");
        }
        this.carteAPiocher = carteAPiocher;
    }

    public void resetCarteAPiocher(){
        carteAPiocher =0;
    }

    private void setUno(boolean uno){
        this.uno = uno;
    }

    public void setaPioche(boolean aPioche) {
        this.aPioche = aPioche;
    }

    /**
     * Le joueur dit Uno.
     * @throws UnoException est lancée si le joueur a plus d'une carte
     * ou si ce n'est pas son tour de jeu.
     */
    public void direUno() throws UnoException {
        if(this.getNombreCarte()>1 || !this.isTourDeJeu()){
            throw new UnoException(this, Partie.NOMBRE_CARTE_PIOCHER_CONTRE_UNO);
        }
        this.uno = true;
    }

    public void newLabel(){
        label = new Label();
    }


    //*********************************Getters*****************************************
    public boolean isUno() {
        return uno;
    }

    public boolean isActionEffectue() {
        return actionEffectue;
    }

    public String getNom() {
        return nom;
    }

    public ArrayList<Carte> getJeu() {
        return jeu;
    }

    public Carte getCarteDuJeu(int indice){
        if(indice <0 || indice > getNombreCarte()-1){
            throw new IllegalArgumentException("indice carte du jeu joueur non valide");
        }
        return getJeu().get(indice);
    }

    public int getNombreCarte(){
        return jeu.size();
    }

    public boolean isTourDeJeu() {
        return tourDeJeu;
    }

    public boolean isaPioche() {
        return aPioche;
    }

    public int getCarteAPiocher() {
        return carteAPiocher;
    }

    public boolean isAttaque(){
        return getCarteAPiocher()>0;
    }

    public Label getLabel() {
        return label;
    }


    //*********************************Méthodes*****************************************
    /**
     * Deux joueurs sont egaux s'ils ont le meme nom.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Joueur joueur = (Joueur) o;
        return Objects.equals(nom, joueur.nom);
    }

    @Override
    public String toString() {
        return "Joueur{" +
                "nom='" + nom + '\'' +
                ", jeu=" + jeu +
                '}';
    }

    /**
     * Ajoute une carte dans le jeu d'un joueur.
     * @param carte La carte ajoutee.
     */
    public void ajouter(Carte carte){
        if (carte == null) {
            throw new IllegalArgumentException("carte null");
        }
        jeu.add(carte);
    }

    /**
     * Le joueur pioche une carte dans la pioche de la partie.
     * Cette carte est ajoutee au jeu du joueur en appelant la methode @ajouter.
     * Le fait de piocher est considere comme une action.
     * Si le joueur avait dit Uno, le champ Uno devient faux.
     *
     * @throws TourException est envoyee si le joueur a deja effectuee une action
     * ou si ce n'est pas son tour de jeu.
     */
    public void pioche()throws TourException{
        Partie partie = Partie.getInstance();
        if(isActionEffectue() || !isTourDeJeu()){
            throw new TourException(this, Partie.NOMBRE_CARTE_PIOCHER_TOUR_INTERDIT);
        }
        if(isAttaque()){
            encaisserAttaque();
            return;
        }
        if(isUno()){
            setUno(false);
        }
        ajouter(partie.pioche());
        setActionEffectue(true);
        setaPioche(true);
        partie.setJoueurSuivant(Partie.getInstance().getJoueurSuivant(this));
    }

    /**
     * Methode appelee lorsque le joueur a une punition suite a un non respect
     * des regles, par ce fait les controles de la méthode @pioche ne sont pas appliquees.
     */
    public void piochePunition(){
        if(isUno()){
            setUno(false);
        }
        ajouter(Partie.getInstance().pioche());
    }

    /**
     * Dans un premier temps, on effectue des tests pour savoir si le joueur a le droit de jouer
     * s'il n'a pas le droit une exception est lancee et le joueur sera puni.
     * - Le joueur est le joueur courant.
     * - Si le joueur a pioche il a le droit de poser uniquement cette carte.
     * - Si le joueur n'a pas deja effectue une action.
     * - Si le joueur est attaque il a le droit de poser uniquement une carte punition.
     *
     * Le joueur pose une carte sur le sommet du tas de la partie, on verifie la legalite du coup
     * en appelant la methode @traiter de la classe Regles.
     * Le fait de poser est considere comme une action.
     *
     * @param carte La carte posee.
     * @throws TourException est lancee si le joueur a deja effectue une action ou si ce n'est pas
     * son tour de jeu.
     * @throws CoupInterditException est lancee si le joueur joue un coup interdit ou si le joueur a pioche une carte
     * et pose une carte differente de celle-ci.
     */
    public void poser(Carte carte) throws TourException, CoupInterditException {
        if(! jeu.contains(carte)){
            throw new IllegalArgumentException("carte non possédée");
        }
        if(!isTourDeJeu()){

            throw new TourException(this, Partie.NOMBRE_CARTE_PIOCHER_TOUR_INTERDIT);
        }
        if(isaPioche() && jeu.indexOf(carte)!=getNombreCarte()-1){
            setaPioche(false);
            throw new CoupInterditException(this, Partie.NOMBRE_CARTE_PIOCHER_COUP_INTERDIT);
        }
        if(isActionEffectue() && !isaPioche()){
            throw new TourException(this, Partie.NOMBRE_CARTE_PIOCHER_TOUR_INTERDIT);
        }
        if(Partie.getInstance().getCarteTas().isPunition() && !carte.isPunition() && isAttaque()){
            encaisserAttaque();
            throw new CoupInterditException(this, Partie.NOMBRE_CARTE_PIOCHER_COUP_INTERDIT);
        }
        try {
            Partie.getInstance().getRegles().traiter(this,carte,Partie.getInstance().getCarteTas());
        } catch (RegleException e) {
            e.printStackTrace();
        }
        setActionEffectue(true);
        jeu.remove(carte);
        if(getNombreCarte()==0){
            Partie.getInstance().retirer(this);
        }
    }

    /**
     * Dans un premier temps, on effectue des tests pour savoir si le joueur a le droit de jouer
     * s'il n'a pas le droit une exception est lancée et le joueur sera puni.
     * - Le joueur est le joueur courant.
     * - Si le joueur a pioche il a le droit de poser uniquement cette carte.
     * - Si le joueur n'a pas deja effectue une action.
     * - Si le joueur est attaque il a le droit de poser uniquement une carte punition.
     *
     * Le joueur pose une carte Joker sur le sommet du tas de la partie.
     * Le fait de poser est considere comme une action.
     *
     * @param carte La carte posee.
     * @param couleur La couleur choisie par le joueur.
     * @throws TourException est lancee si le joueur a deja effectue une action ou si ce n'est pas
     * son tour de jeu.
     * @throws CoupInterditException est lancee si le joueur joue un coup interdit ou si le joueur a pioche une carte
     * et pose une carte differente de celle-ci.
     */
    public void poser(Carte carte, Couleur couleur) throws TourException,CoupInterditException {
        if(! jeu.contains(carte)){
            throw new IllegalArgumentException("carte non possédée");
        }
        if(!isTourDeJeu()){
            throw new TourException(this, Partie.NOMBRE_CARTE_PIOCHER_TOUR_INTERDIT);
        }
        if(isaPioche() && jeu.indexOf(carte)!=getNombreCarte()-1){
            setaPioche(false);
            throw new CoupInterditException(this, Partie.NOMBRE_CARTE_PIOCHER_COUP_INTERDIT);
        }
        if(isActionEffectue()){
            throw new TourException(this, Partie.NOMBRE_CARTE_PIOCHER_TOUR_INTERDIT);
        }
        if(Partie.getInstance().getCarteTas().isPunition() && !carte.isPunition() && isAttaque()){
            encaisserAttaque();
        }
        carte.setCouleurJoker(couleur.getNomCouleur());
        try {
            Partie.getInstance().getRegles().traiter(this,carte,Partie.getInstance().getCarteTas());
        } catch (RegleException e) {
            e.printStackTrace();
        }
        setActionEffectue(true);
        jeu.remove(carte);
        if(getNombreCarte()==0){
            Partie.getInstance().retirer(this);
        }
    }

    /**
     * Le joueur pioche @getCarteAPiocher suite à une attaque et finit
     * son tour automatiquement.
     */
    public void encaisserAttaque(){
        for (int i = 0; i < getCarteAPiocher(); i++) {
            piochePunition();
        }
        resetCarteAPiocher();
        Partie.getInstance().setJoueurSuivant(Partie.getInstance().getJoueurSuivant(this));
//        System.out.println((Partie.getInstance().getJoueurSuivant(this)));
        finDeTourAutomatique();
    }

    public void encaisserAttaquePlus4(){
        for (int i = 0; i < getCarteAPiocher(); i++) {
            piochePunition();
        }
        resetCarteAPiocher();
    }

    /**
     * Le joueur finit son tour.
     *
     * @throws UnoException est lancee si le joueur possede une seule carte et oublie de dire uno.
     * @throws TourException est lancee si le joueur n'est pas le joueur courant ou s'il n'a pas effectue
     * d'action durant son tour.
     */
    public void finDeTour() throws UnoException,TourException{
        if(!isTourDeJeu() ){
            throw new TourException(this, Partie.NOMBRE_CARTE_PIOCHER_TOUR_INTERDIT);
        }
        if(isAttaque()){
            encaisserAttaque();
            return;
        }
        if(!isActionEffectue()){
            setTourDeJeu(false);
            Partie.getInstance().setJoueurSuivant(Partie.getInstance().getJoueurSuivant(this));
            Partie.getInstance().getJoueurSuivant().setTourDeJeu(true);
            throw new TourException(this, Partie.NOMBRE_CARTE_PIOCHER_TOUR_INTERDIT);
        }
        if(isaPioche()){
            Partie.getInstance().setJoueurSuivant(Partie.getInstance().getJoueurSuivant());
            setaPioche(false);
        }
        setTourDeJeu(false);
        Partie.getInstance().getJoueurSuivant().setTourDeJeu(true);
        setActionEffectue(false);
        if(getNombreCarte()==1 && !isUno()){
            throw new UnoException(this, Partie.NOMBRE_CARTE_PIOCHER_CONTRE_UNO);
        }
    }

    /**
     * Le joueur finit son tour suite a une attaque.
     */
    public void finDeTourAutomatique(){
        setActionEffectue(false);
        Partie.getInstance().getJoueurSuivant().setTourDeJeu(true);
        setTourDeJeu(false);
    }
}
