package back.parsage.parser;


import back.carte.CarteSimple;
import back.partie.Partie;

/**
 * Classe reprensentant le parser d'une carte simple.
 */
public class ParserCarteSimple extends Parser {

    //*********************************Constructeur*****************************************
    public ParserCarteSimple(Parser suivant) {
        super(suivant);
    }

    //*********************************Méthodes*****************************************

    /**
     * Cree une carte simple et l'ajoute a la pioche.
     * @param ligne la ligne a parsee.
     */
    @Override
    public void parser(String ligne)  {
        String[] parsage  = ligne.split(";");
        int valeur = Integer.parseInt(parsage[2]);
        CarteSimple carteSimple = new CarteSimple(parsage[1],valeur);
        Partie.getInstance().ajouterCartePioche(carteSimple);
    }

    /**
     *
     * @param ligne la ligne a traitee.
     * @return vrai si la ligne contient "CarteSimple"
     */
    @Override
    public boolean saitParser(String ligne) {
        return ligne.contains("CarteSimple");
    }
}
