package back.parsage.parser;


import back.carte.CartePasser;
import back.partie.Partie;

/**
 * Classe reprensentant le parser d'une carte passer.
 */
public class ParserCartePasser extends Parser{
    //*********************************Constructeur*****************************************
    public ParserCartePasser(Parser suivant) {
        super(suivant);
    }

    //*********************************Méthodes*****************************************

    /**
     * Cree une carte passer et l'ajoute a la pioche.
     * @param ligne la ligne a parsee.
     */
    @Override
    public void parser(String ligne)  {
        String[] parsage  = ligne.split(";");
        CartePasser cartePasser = new CartePasser(parsage[1]);
        Partie.getInstance().ajouterCartePioche(cartePasser);
    }

    /**
     *
     * @param ligne la ligne a traitee.
     * @return vrai si la ligne contient "CartePasser"
     */
    @Override
    public boolean saitParser(String ligne) {
        return ligne.contains("CartePasser");
    }
}
