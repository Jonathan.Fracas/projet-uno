package back.parsage.parser;


import back.carte.CarteCouleur;
import back.partie.Partie;

/**
 * Classe reprensentant le parser d'une carte couleur.
 */
public class ParserCarteCouleur extends Parser{
    //*********************************Constructeur*****************************************
    public ParserCarteCouleur(Parser suivant) {
        super(suivant);
    }

    //*********************************Méthodes*****************************************

    /**
     * Cree une carte couleur et l'ajoute a la pioche.
     * @param ligne la ligne a parsee.
     */
    @Override
    public void parser(String ligne)  {
        String[] parsage  = ligne.split(";");
        CarteCouleur carteCouleur = new CarteCouleur();
        Partie.getInstance().ajouterCartePioche(carteCouleur);
    }

    /**
     *
     * @param ligne la ligne a traitee.
     * @return vrai si la ligne contient "CarteCouleur"
     */
    @Override
    public boolean saitParser(String ligne) {
        return ligne.contains("CarteCouleur");
    }
}
