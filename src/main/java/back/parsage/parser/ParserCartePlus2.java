package back.parsage.parser;


import back.carte.CartePlus2;
import back.partie.Partie;

/**
 * Classe reprensentant le parser d'une carte plus 2.
 */
public class ParserCartePlus2 extends Parser{
    //*********************************Constructeur*****************************************
    public ParserCartePlus2(Parser suivant) {
        super(suivant);
    }

    //*********************************Méthodes*****************************************

    /**
     * Cree une carte plus 2 et l'ajoute a la pioche.
     * @param ligne la ligne a parsee.
     */
    @Override
    public void parser(String ligne)  {
        String[] parsage  = ligne.split(";");
        CartePlus2 cartePlus2 = new CartePlus2(parsage[1],true);
        Partie.getInstance().ajouterCartePioche(cartePlus2);
    }

    /**
     *
     * @param ligne la ligne a traitee.
     * @return vrai si la ligne contient "CartePlus2".
     */
    @Override
    public boolean saitParser(String ligne) {
        return ligne.contains("CartePlus2");
    }
}
