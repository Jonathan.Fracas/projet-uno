package back.parsage.parser;

public class ParserManquantException extends Exception{
    //*********************************Constructeur*****************************************

    /**
     * Constructeur.
     * @param msg message affiche en sortie.
     */
    public ParserManquantException(String msg) {
        super(msg);
    }

}
