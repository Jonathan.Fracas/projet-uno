package back.parsage.parser;


/**
 * Classe (abstraite) du parser.
 */
public abstract class Parser {
	//*********************************Champ*****************************************
	private Parser suivant = null;

	//*********************************Constructeur*****************************************
	public Parser(Parser suivant) {
		this.suivant = suivant;
	}

	//*********************************Getters*****************************************
	private Parser getSuivant() {
		return suivant;
	}

	private boolean aUnSuivant() {
		return suivant != null;
	}

	//*********************************Méthodes*****************************************
	/**
	 * La fonction traiter() parcours la liste à la recherche d'un maillon qui sait comment parser
	 * la ligne. Dans ce cas la ligne est parsée et la recherche s'arrete
	 * @param ligne la ligne a parser
	 * @exception ParserManquantException si la carte n'a pas de parseur.
	 */
	public void traiter(String ligne) throws Exception {
		if (saitParser(ligne))
			parser(ligne);
		else if (aUnSuivant())
			getSuivant().traiter(ligne);
		else
			throw new ParserManquantException("Pas de parser pour cette ligne"+ligne);

	}

	/**
	 * Parse une ligne.
	 * @param ligne la ligne a parsee.
	 */
	public abstract void parser(String ligne);
	
	/**
	 * Renvoie true si le parser en question reconnait le type de ligne, c'est-a-dire
	 * qu'il sait la "decortiquer", et créer le ou les objets qu'il faut. Il n'y a pas
	 * d'exception. En cas de probleme, on renvoie false !
	 * @param ligne la ligne a traitee.
	 * @return true si la ligne est reconnue
	 */
	public abstract boolean saitParser(String ligne);
}
