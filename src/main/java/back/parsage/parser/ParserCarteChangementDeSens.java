package back.parsage.parser;


import back.carte.CarteChangementDeSens;
import back.partie.Partie;

/**
 * Classe reprensentant le parser d'une carte changement de sens
 */
public class ParserCarteChangementDeSens extends Parser{
    //*********************************Constructeur*****************************************
    public ParserCarteChangementDeSens(Parser suivant) {
        super(suivant);
    }

    //*********************************Méthodes*****************************************

    /**
     * Cree une carte changement de sens et l'ajoute a la pioche de la partie.
     * @param ligne la ligne a parsee.
     */
    @Override
    public void parser(String ligne)  {
        String[] parsage  = ligne.split(";");
        CarteChangementDeSens carteChangementDeSens = new CarteChangementDeSens(parsage[1]);
        Partie.getInstance().ajouterCartePioche(carteChangementDeSens);
    }

    /**
     *
     * @param ligne la ligne a traitee.
     * @return vrai si la ligne contient "CarteChangementDeSens"
     */
    @Override
    public boolean saitParser(String ligne) {
        return ligne.contains("CarteChangementDeSens");
    }

}
