package back.parsage.parser;


import back.carte.CartePlus4;
import back.partie.Partie;

/**
 * Classe reprensentant le parser d'une carte plus 4.
 */
public class ParserCartePlus4 extends Parser{
    //*********************************Constructeur*****************************************
    public ParserCartePlus4(Parser suivant) {
        super(suivant);
    }

    //*********************************Méthodes*****************************************

    /**
     * Cree une carte plus 4 et l'ajoute a la pioche.
     * @param ligne la ligne a parsee.
     */
    @Override
    public void parser(String ligne)  {
        String[] parsage  = ligne.split(";");
        CartePlus4 cartePlus4 = new CartePlus4();
        Partie.getInstance().ajouterCartePioche(cartePlus4);
    }

    /**
     *
     * @param ligne la ligne a traitee.
     * @return vrai si la ligne contient "CartePlus4"
     */
    @Override
    public boolean saitParser(String ligne) {
        return ligne.contains("CartePlus4");
    }
}
