package back.parsage;

import back.parsage.parser.Parser;
import back.parsage.parser.ParserManquantException;
import back.partie.Partie;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Classe qui cree un jeu de carte depuis un fichier csv contenant des lignes formatees correspondant
 * a des cartes.
 */
public class JeuDeCarte {
    //*********************************Champ*****************************************
    /**
     * le nom du fichier a parser.
     */
    private String nomDuFichier;

    //*********************************Constructeur*****************************************
    public JeuDeCarte(String nomDuFichier) {
        setNomDuFichier(nomDuFichier);

        this.nomDuFichier = nomDuFichier;
    }

    private void setNomDuFichier(String nomDuFichier) {
        if (nomDuFichier == null) {
            throw new IllegalArgumentException("fichier null");
        }
        try {
            lire(nomDuFichier, null,null);
//            System.out.println("Ouverture du fichier réussi");
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }
    }

    //*********************************Getter*****************************************
    public String getNomDuFichier() {
        return nomDuFichier;
    }


    //*********************************Méthodes*****************************************
    /**
     * Cette methode ouvre le fichier (en faisant tous les controles necessaires). Puis, elle boucle sur
     * chaque ligne et confie au parser le traitement de la ligne. S'il n'y a pas de parser,
     * on affiche la carte qui pose problème.
     *
     * @param nomFichier nom du fichier à lire et à parser
     * @param parser c'est le premier parser de la liste
     * @param partie la partie de Uno en cours
     */
    public static void lire(String nomFichier, Parser parser, Partie partie) {
        File fichier = new File(nomFichier);

        if (! fichier.isFile())
            throw new IllegalArgumentException("Ce fichier n'existe pas");

        BufferedReader reader = null;
        String ligne;

        try {
            reader = new BufferedReader(new FileReader(fichier));

            while ((ligne = reader.readLine()) != null) {
                // On a bien lu une ligne ue fichier
                if (parser==null){
                    // Si y a pas de parser, alors on ne fait rien
                }
                else
                    // Puisqu'on a un parser, on l'utilise. C'est lui qui traitera la ligne
                    // pour créer les cartes
                    try {
                        parser.traiter(ligne);
                    }
                    catch (ParserManquantException e) {
                        System.err.println("Aucun parser n'existe pour la ligne "+ligne);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
