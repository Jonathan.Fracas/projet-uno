package back.test;


import back.carte.CarteSimple;
import back.exception.CoupInterditException;
import back.exception.TourException;
import back.exception.UnoException;
import back.joueur.Joueur;
import back.parsage.JeuDeCarte;
import back.partie.Partie;

/**
 * Classe testant la carte changement de sens.
 */
public class TestCoupChangementSens {
    public static final void testCoupChangementSens()throws Exception{
        Partie partie = Partie.getInstance();
        partie.reset();
        JeuDeCarte jeuDeCarte = new JeuDeCarte("jeux_test/JeuTestCarteChangementDeSens.csv");

        partie.initialiserRegle();
        partie.initialiser(jeuDeCarte);

        Joueur Alice = new Joueur("Alice",true);
        Joueur Bob = new Joueur("Bob");
        Joueur Charles = new Joueur("Charles");
        partie.ajouter(Alice);
        partie.ajouter(Bob);
        partie.ajouter(Charles);

        partie.distributionCarte(3);

//        System.out.println(Alice);
//        System.out.println(Bob);
//        System.out.println(Charles);
//        System.out.println("Tas : "+partie.getTas());

        int cpt=0;

        if (!partie.getJoueurCourant().equals(Alice)) {
            System.out.println("pbm alice pas joueur courant");cpt+=1;
        }
        try {
            Alice.poser(Alice.getCarteDuJeu(0));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try {
            Alice.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }
        if(!partie.getJoueurCourant().equals(Charles)) {
            System.out.println("pbm charles pas joueur courant");cpt+=1;
        }
        try {
            Charles.poser(Charles.getCarteDuJeu(1));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try {
            Charles.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }
        if (!partie.getJoueurCourant().equals(Alice)) {
            System.out.println("pbm alice pas joueur courant");cpt+=1;
        }
        try {
            Alice.poser(Alice.getCarteDuJeu(0));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try {
            Alice.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }catch (UnoException e){
            e.punirJoueur();
        }
        if (Alice.getNombreCarte()!=3) {
            System.out.println("pbm alice n'a pas 3 cartes");cpt+=1;
        }
        if (!partie.getJoueurCourant().equals(Bob)) {
            System.out.println("pbm bob pas joueur courant");cpt+=1;
        }
        try {
            Bob.poser(Bob.getCarteDuJeu(2));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try {
            Bob.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }
        if(!partie.getJoueurCourant().equals(Charles)) {
            System.out.println("pbm charles pas joueur courant");cpt+=1;
        }

        partie.reset();
        Joueur Alice1 = new Joueur("Alice",true);
        Joueur Bob1 = new Joueur("Bob");
        Joueur Charles1 = new Joueur("Charles");
        partie.ajouter(Alice1);
        partie.ajouter(Bob1);
        partie.ajouter(Charles1);
        partie.initialiser(jeuDeCarte);
        partie.distributionCarte(3);
        partie.initialiserRegle();

        if(!partie.getJoueurCourant().equals(Alice1)) {
            System.out.println("pbm alice1 pas joueur courant");cpt+=1;
        }
        try {
            Alice1.poser(Alice1.getCarteDuJeu(1));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try {
            Alice1.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }catch (UnoException e){
            e.punirJoueur();
        }
        if(!partie.getJoueurCourant().equals(Bob1)) {
            System.out.println("pbm bob1 pas joueur courant");cpt+=1;
        }
        try {
            Bob1.poser(Bob1.getCarteDuJeu(0));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try {
            Bob1.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }catch (UnoException e){
            e.punirJoueur();
        }
        if (!partie.getJoueurCourant().equals(Charles1)) {
            System.out.println("pbm charles1 pas joueur courant");cpt+=1;
        }
        try {
            Charles1.poser(Charles1.getCarteDuJeu(1));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        if(Charles1.getNombreCarte()!=5) {
            System.out.println("pbm charles1 n'a pas 5 cartes");cpt+=1;
        }
        if(!partie.getCarteTas().equals(new CarteSimple("Jaune",9))) {
            System.out.println("pbm carte tas pas jaune 9");cpt+=1;
        }
        if (!partie.getJoueurCourant().equals(Alice1)) {
            System.out.println("pbm alice1 pas joueur courant");cpt+=1;
        }
        if(!partie.getCartePioche(0).equals(new CarteSimple("Vert",2))) {
            System.out.println("pbm carte sommet pioche pas vert 2");cpt+=1;
        }

        if(cpt==0){
            System.out.println("Test carte changement de sens ok");
        }else{
            System.out.println(cpt+" erreur(s) dans test carte changement de sens");
        }

    }
}
