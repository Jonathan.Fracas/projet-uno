package back.test;

import back.carte.CartePasser;
import back.carte.CarteSimple;
import back.exception.CoupInterditException;
import back.exception.TourException;
import back.joueur.Joueur;
import back.parsage.JeuDeCarte;
import back.partie.Partie;


/**
 * Classe testant la carte passer.
 */
public class TestCoupPasseTour {
    public final static void testCoupPasseTour() throws Exception{
        Partie partie = Partie.getInstance();
        partie.reset();
        JeuDeCarte jeuDeCarte = new JeuDeCarte("jeux_test/JeuTestCartePasser.csv");

        partie.initialiserRegle();

        partie.initialiser(jeuDeCarte);
        Joueur Alice = new Joueur("Alice",true);
        Joueur Bob = new Joueur("Bob");
        Joueur Charles = new Joueur("Charles");
        partie.ajouter(Alice);
        partie.ajouter(Bob);
        partie.ajouter(Charles);

        partie.distributionCarte(3);

//        System.out.println(Alice);
//        System.out.println(Bob);
//        System.out.println(Charles);
//        System.out.println(partie.getTas());

        int cpt=0;

        if (!partie.getJoueurCourant().equals(Alice)) {
            System.out.println("pbm alice pas joueur courant");
            cpt += 1;
        }
        try {
            Alice.poser(Alice.getJeu().get(0));
        } catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try{
            Alice.finDeTour();
        } catch (TourException e){
            e.punirJoueur();
        }
        if(!partie.getJoueurCourant().equals(Charles)) {
            System.out.println("pbm charles pas joueur courant");
            cpt += 1;
        }
        CartePasser rougePasser = new CartePasser("Rouge");
        if(!partie.getCarteTas().equals(rougePasser)) {
            System.out.println("pbm carte tas pas rouge passe");
            cpt += 1;
        }

        try {
            Charles.poser(Charles.getJeu().get(1));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try{
            Charles.finDeTour();
        } catch (TourException e){
            e.punirJoueur();
        }

        if(!partie.getJoueurCourant().equals(Bob)) {
            System.out.println("pbm bob pas joueur courant");
            cpt += 1;
        }
        CartePasser vertPasser = new CartePasser("Vert");
        if(!partie.getCarteTas().equals(vertPasser)) {
            System.out.println("pbm carte tas pas vert passe");
            cpt += 1;
        }

        try {
            Bob.poser(Bob.getJeu().get(1));
        } catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try{
            Bob.finDeTour();
        } catch (TourException e){
            e.punirJoueur();
        }

        if(!partie.getJoueurCourant().equals(Charles)) {
            System.out.println("pbm charles pas joueur courant");
            cpt += 1;
        }
        CarteSimple vert6 = new CarteSimple("Vert",6);
        if(!partie.getCarteTas().equals(vert6)) {
            System.out.println("pbm carte tas pas 6 vert");
            cpt += 1;
        }


        partie.reset();
        Joueur Alice1 = new Joueur("Alice",true);
        Joueur Bob1 = new Joueur("Bob");
        Joueur Charles1 = new Joueur("Charles");
        partie.ajouter(Alice1);
        partie.ajouter(Bob1);
        partie.ajouter(Charles1);
        partie.initialiser(jeuDeCarte);
        partie.distributionCarte(3);
        partie.initialiserRegle();

        if (!partie.getJoueurCourant().equals(Alice1)) {
            System.out.println("pbm alice1 pas joueur courant");
            cpt += 1;
        }
        try {
            Alice1.poser(Alice1.getJeu().get(0));
        } catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try{
            Alice1.finDeTour();
        } catch (TourException e){
            e.punirJoueur();
        }

        if(!partie.getJoueurCourant().equals(Charles1)) {
            System.out.println("pbm charles1 pas joueur courant");
            cpt += 1;
        }
        if(!partie.getCarteTas().equals(rougePasser)) {
            System.out.println("pbm carte tas pas rouge passe");
            cpt += 1;
        }

        try {
            Charles1.poser(Charles1.getJeu().get(0));
            cpt++;
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
            if(Charles1.getNombreCarte()!=5) {
                System.out.println("pbm charles1 pas 5 cartes");
                cpt += 1;
            }
        }
        try{
            Charles1.finDeTour();
        } catch (TourException e){
            e.punirJoueur();
        }


        partie.reset();
        Joueur Alice2 = new Joueur("Alice",true);
        Joueur Bob2 = new Joueur("Bob");
        Joueur Charles2 = new Joueur("Charles");
        partie.ajouter(Alice2);
        partie.ajouter(Bob2);
        partie.ajouter(Charles2);
        partie.initialiser(jeuDeCarte);
        partie.distributionCarte(3);
        partie.initialiserRegle();

        if(!partie.getJoueurCourant().equals(Alice2)) {
            System.out.println("pbm alice2 pas joueur courant");
            cpt += 1;
        }
        try {
            Alice2.poser(Alice2.getJeu().get(1));
        } catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try{
            Alice2.finDeTour();
        } catch (TourException e){
            e.punirJoueur();
        }
        try {
            Bob2.poser(Bob2.getJeu().get(1));
        } catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try{
            Bob2.finDeTour();
        } catch (TourException e){
            e.punirJoueur();
        }
        if(!partie.getJoueurCourant().equals(Charles2)) {
            System.out.println("pbm charles2 pas joueur courant");
            cpt += 1;
        }
        if(Charles2.getNombreCarte()!=3) {
            System.out.println("pbm charles2 n'a pas 3 cartes");
            cpt += 1;
        }
        try {
            Charles2.poser(Charles2.getJeu().get(1));
            cpt++;
        } catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
            if(Charles2.getNombreCarte()!=5) {
                System.out.println("pbm charles2 n'a pas 5 cartes");
                cpt += 1;
            }
        }
        try{
            Charles2.finDeTour();
        } catch (TourException e){
            e.punirJoueur();
        }

        if(cpt==0){
            System.out.println("Test carte passe tour ok");
        }else{
            System.out.println(cpt+" erreur(s) dans test carte passe tour");
        }

    }
}
