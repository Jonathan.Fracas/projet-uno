package back.test;

import back.exception.CoupInterditException;
import back.exception.TourException;
import back.joueur.Joueur;
import back.parsage.JeuDeCarte;
import back.partie.Partie;


/**
 * Classe testant la carte plus 2.
 */
public class TestCoupCartePlus2{
    public final static void testCoupCartePlus2() throws Exception{
        Partie partie = Partie.getInstance();
        partie.reset();
        JeuDeCarte jeuDeCarte = new JeuDeCarte("jeux_test/JeuTestCartePlus2.csv");

        partie.initialiserRegle();

        partie.initialiser(jeuDeCarte);
        Joueur Alice = new Joueur("Alice",true);
        Joueur Bob = new Joueur("Bob");
        Joueur Charles = new Joueur("Charles");
        partie.ajouter(Alice);
        partie.ajouter(Bob);
        partie.ajouter(Charles);

        partie.distributionCarte(3);

//        System.out.println(Alice);
//        System.out.println(Bob);
//        System.out.println(Charles);
//        System.out.println(partie.getTas());

        int cpt = 0;

        if(!partie.getJoueurCourant().equals(Alice)) {
            System.out.println("pbm alice pas joueur courant");
            cpt+=1;
        }
        try {
            Alice.poser(Alice.getJeu().get(0));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try {
            Alice.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }
        if(!partie.getJoueurCourant().equals(Bob)) {
            System.out.println("pbm bob pas joueur courant");
            cpt+=1;
        }
        if(Bob.getNombreCarte()!=3) {
            System.out.println("pbm bob n'a pas 3 cartes");
            cpt+=1;
        }
        Bob.encaisserAttaque();
        if(Bob.getNombreCarte()!=5){
            System.out.println("pbm bob n'a pas 5 cartes");
            cpt+=1;
        }
        if(!partie.getJoueurCourant().equals(Charles)){
            System.out.println("pbm charles pas joueur courant");
            cpt+=1;
        }
        try {
            Charles.poser(Charles.getJeu().get(2));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try {
            Charles.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }
        if(Charles.getNombreCarte()!=2) {
            System.out.println("pbm charles n'a pas 2 cartes");
            cpt+=1;
        }


        partie.reset();
        Joueur Alice1 = new Joueur("Alice",true);
        Joueur Bob1 = new Joueur("Bob");
        Joueur Charles1 = new Joueur("Charles");
        partie.ajouter(Alice1);
        partie.ajouter(Bob1);
        partie.ajouter(Charles1);
        partie.initialiser(jeuDeCarte);
        partie.distributionCarte(3);
        partie.initialiserRegle();

        if(!partie.getJoueurCourant().equals(Alice1)) {
            System.out.println("pbm alice1 pas joueur courant 1");
            cpt+=1;
        }
        try {
            Alice1.pioche();
        }catch (TourException e){
            e.punirJoueur();
        }
        if(Alice1.getNombreCarte()!=4){
            System.out.println("pbm alice1 pas 4 cartes");
            cpt+=1;
        }
        try {
            Alice1.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }
        if(!partie.getJoueurCourant().equals(Bob1)){
            System.out.println("pbm bob1 pas joueur courant 1");
            cpt+=1;
        }
        try {
            Bob1.pioche();
        }catch (TourException e){
            e.punirJoueur();
        }
        try {
            Bob1.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }
        if(!partie.getJoueurCourant().equals(Charles1)){
            System.out.println("pbm charles1 pas joueur courant 1");
            cpt+=1;
        }
        try {
            Charles1.poser(Charles1.getJeu().get(1));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try {
            Charles1.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }
        if(!partie.getJoueurCourant().equals(Alice1)){
            System.out.println("pbm alice1 pas joueur courant 2");
            cpt+=1;
        }
        try {
            Alice1.poser(Alice1.getJeu().get(0));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try {
            Alice1.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }
        if(!partie.getJoueurCourant().equals(Bob1)){
            System.out.println("pbm bob1 pas joueur courant 2");
            cpt+=1;
        }
        if(Bob1.getNombreCarte()!=4){
            System.out.println("pbm bob1 n'a pas 4 cartes");
            cpt+=1;
        }
        Bob1.encaisserAttaque();
        if(Bob1.getNombreCarte()!=8){
            System.out.println("pbm bob1 n'a pas 8 cartes");
            cpt+=1;
        }
        if(!partie.getJoueurCourant().equals(Charles1)) {
            System.out.println("pbm charles1 pas joueur courant 2");
            cpt+=1;
        }

        if(cpt==0){
            System.out.println("Test carte plus 2 ok");
        }else{
            System.out.println(cpt+" erreur(s) dans test carte plus 2");
        }
    }
}
