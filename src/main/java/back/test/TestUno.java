package back.test;


import back.carte.CarteSimple;
import back.exception.CoupInterditException;
import back.exception.TourException;
import back.exception.UnoException;
import back.joueur.Joueur;
import back.parsage.JeuDeCarte;
import back.partie.Partie;

/**
 * Classe testant les mecaniques du Uno.
 */
public class TestUno {
    public final static void testUno() throws Exception{
        Partie partie = Partie.getInstance();
        partie.reset();
        JeuDeCarte jeuDeCarte = new JeuDeCarte("jeux_test/JeuTestCarteSimplePourTestUno.csv");

        partie.initialiserRegle();

        partie.initialiser(jeuDeCarte);
        Joueur Alice = new Joueur("Alice",true);
        Joueur Bob = new Joueur("Bob");
        Joueur Charles = new Joueur("Charles");
        partie.ajouter(Alice);
        partie.ajouter(Bob);
        partie.ajouter(Charles);

        partie.distributionCarte(2);

        int cpt=0;

//        System.out.println(Alice);
//        System.out.println(Bob);
//        System.out.println(Charles);
//        System.out.println(partie.getTas());

        if(Alice.getNombreCarte()!=2) {
            System.out.println("pbm alice n'a pas 2 cartes");
            cpt += 1;
        }
        try {
            Alice.poser(Alice.getJeu().get(0));
        } catch (CoupInterditException e) {
            e.punirJoueur();
        } catch (TourException e){
            e.punirJoueur();
        }
        try {
            Alice.direUno();
        } catch (UnoException e) {
            e.punirJoueur();
        }
        try {
            Alice.finDeTour();
        } catch (TourException e) {
            e.punirJoueur();
        }catch (UnoException e){
            e.punirJoueur();
        }
        if(Alice.getNombreCarte()!=1) {
            System.out.println("pbm alice n'a pas 1 cartes");
            cpt += 1;
        }
//        System.out.println(partie.getCarteTas());
        CarteSimple vert2 = new CarteSimple("Vert",2);
        if(!partie.getCarteTas().equals(vert2)) {
            System.out.println("pbm carte du tas pas 2 vert");
            cpt += 1;
        }
        if (!Bob.isTourDeJeu()) {
            System.out.println("pbm bob pas joueur courant");
            cpt += 1;
        }


        partie.reset();
        Joueur Alice1 = new Joueur("Alice",true);
        Joueur Bob1 = new Joueur("Bob");
        Joueur Charles1 = new Joueur("Charles");
        partie.ajouter(Alice1);
        partie.ajouter(Bob1);
        partie.ajouter(Charles1);
        partie.initialiser(jeuDeCarte);
        partie.distributionCarte(2);
        partie.initialiserRegle();

//        System.out.println(Alice1);
//        System.out.println(Bob1);
//        System.out.println(Charles1);
//        System.out.println("Tas : "+partie1.getTas());

        try {
            Alice1.poser(Alice1.getJeu().get(0));
        } catch (CoupInterditException e) {
            e.punirJoueur();
        } catch (TourException e){
            e.punirJoueur();
        }
        try {
            Alice1.finDeTour();
            System.out.println("pbm zlice passe son tour sans dire uno et n'est pas puni");
            cpt++;
        } catch (TourException e) {
            e.punirJoueur();
        }catch (UnoException e){
            e.punirJoueur();
            if(Alice1.getNombreCarte()!=3) {
                System.out.println("pbm alice1 n'a pas 3 cartes");
                cpt += 1;
            }
//            System.out.println(Alice1);
//            System.out.println(partie1.getCarteTas());
//            CarteSimple vert2 = new CarteSimple("Vert",2);
            if(!partie.getCarteTas().equals(vert2)) {
                System.out.println("pbm carte du tas pas 2 vert");
                cpt += 1;
            }
            if(!Bob1.isTourDeJeu()) {
                System.out.println("pbm bob1 pas joueur courant");
                cpt += 1;
            }
        }


        partie.reset();
        Joueur Alice2 = new Joueur("Alice",true);
        Joueur Bob2 = new Joueur("Bob");
        Joueur Charles2 = new Joueur("Charles");
        partie.ajouter(Alice2);
        partie.ajouter(Bob2);
        partie.ajouter(Charles2);
        partie.initialiser(jeuDeCarte);
        partie.distributionCarte(2);
        partie.initialiserRegle();

//        System.out.println(Alice2);
//        System.out.println(Bob2);
//        System.out.println(Charles2);
//        System.out.println("Tas : "+partie2.getTas());

        if(!Alice2.isTourDeJeu()) {
            System.out.println("pbm alice2 pas joueur courant");
            cpt += 1;
        }
        try {
            Bob2.direUno();
            System.out.println("pbm bob dit uno alors que ce n'est pas son tour");
            cpt++;
        }catch (UnoException e){
            e.punirJoueur();
            if (Bob2.getNombreCarte()!=4) {
                System.out.println("pbm bob2 n'a pas 4 cartes");
                cpt += 1;
            }
            if(!Alice2.isTourDeJeu()) {
                System.out.println("pbm alice2 pas joueur courant");
                cpt += 1;
            }
            CarteSimple vert8 = new CarteSimple("Vert",8);
            if (!partie.getCarteTas().equals(vert8)) {
                System.out.println("pbm carte tas pas 8 vert");
                cpt += 1;
            }
//            System.out.println(partie2.getCarteTas());
        }

        if(cpt==0){
            System.out.println("Test uno ok");
        }else{
            System.out.println(cpt+" erreur(s) dans test uno");
        }

    }
}
