package back.test;

import back.carte.Carte;
import back.carte.Couleur;
import back.exception.CoupInterditException;
import back.exception.TourException;
import back.exception.UnoException;
import back.joueur.Joueur;
import back.parsage.JeuDeCarte;
import back.partie.Partie;


/**
 * Classe testant la carte couleur.
 */
public class TestCoupCarteCouleur {
    public static final void testCoupCarteCouleur()throws Exception{
        Partie partie = Partie.getInstance();
        partie.reset();
        JeuDeCarte jeuDeCarte = new JeuDeCarte("jeux_test/JeuTestCarteCouleur.csv");

        partie.initialiserRegle();
        partie.initialiser(jeuDeCarte);

//        for (Carte carte:partie.getPioche()
//             ) {
//            System.out.println(carte.getImageURL());
//        }

        Joueur Alice = new Joueur("Alice",true);
        Joueur Bob = new Joueur("Bob");
        Joueur Charles = new Joueur("Charles");
        partie.ajouter(Alice);
        partie.ajouter(Bob);
        partie.ajouter(Charles);

        partie.distributionCarte(3);

//        System.out.println(Alice);
//        System.out.println(Bob);
//        System.out.println(Charles);
//        System.out.println("Tas : "+partie.getTas());

        int cpt = 0;

        if(!partie.getJoueurCourant().equals(Alice)) {
            System.out.println("pbm alice pas joueur courant");
            cpt+=1;
        }
        try {
            Alice.poser(Alice.getCarteDuJeu(0), Couleur.BLEU);
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try {
            Alice.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }catch (UnoException e){
            e.punirJoueur();
        }
        if(Alice.getNombreCarte()!=2){
            System.out.println("pbm alice pas 2 carte");
            cpt+=1;
        }
        if(!partie.getJoueurCourant().equals(Bob)){
            System.out.println("pbm bob pas joueur courant");
            cpt+=1;
        }
        try {
            Bob.poser(Bob.getCarteDuJeu(2));
        }catch (TourException e){
            e.punirJoueur();
        }catch (CoupInterditException e){
            e.punirJoueur();
        }
        try {
            Bob.finDeTour();
        }catch (TourException e){
            e.punirJoueur();
        }catch (UnoException e){
            e.punirJoueur();
        }
        if(Bob.getNombreCarte()!=2) {
            System.out.println("pbm bob pas 2 carte");
            cpt+=1;
        }
        if(!partie.getJoueurCourant().equals(Charles)) {
            System.out.println("pbm charles pas joueur courant");
            cpt+=1;
        }

        if(cpt==0){
            System.out.println("Test carte couleur ok");
        }else{
            System.out.println(cpt+" erreur(s) dans test carte couleur");
        }


    }
}
