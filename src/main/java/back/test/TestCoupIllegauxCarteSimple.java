package back.test;


import back.carte.CarteSimple;
import back.joueur.Joueur;
import back.parsage.JeuDeCarte;
import back.partie.Partie;

/**
 * Classe testant les coups illegaux avec une carte simple.
 */
public class TestCoupIllegauxCarteSimple {
    public final static void testCoupIllegauxCarteSimple() {

        Partie partie = Partie.getInstance();
        partie.reset();
        JeuDeCarte jeuDeCarte = new JeuDeCarte("jeux_test/JeuTestCarteSimple.csv");

        partie.initialiserRegle();

        partie.initialiser(jeuDeCarte);
        Joueur Alice = new Joueur("Alice",true);
        Joueur Bob = new Joueur("Bob");
        Joueur Charles = new Joueur("Charles");
        partie.ajouter(Alice);
        partie.ajouter(Bob);
        partie.ajouter(Charles);
        partie.setPremierJoueur(Alice);


        partie.distributionCarte(3);

        int cpt=0;

//        System.out.println(Alice);
//        System.out.println(Bob);
//        System.out.println(Charles);
//        System.out.println(partie.getTas());



        try {
            Alice.poser(Alice.getJeu().get(1));
            cpt++;
        }catch (Exception e){
            if(Alice.getNombreCarte()!=3) {
                System.out.println("pbm alice n'a pas 3 cartes");cpt+=1;
            }
            CarteSimple jaune6 = new CarteSimple("Jaune",6);
            if(!Alice.getCarteDuJeu(1).equals(jaune6)) {
                System.out.println("pbm alice n'a pas 6 jaune");cpt+=1;
            }
//            System.out.println(Alice);
        }


        partie.reset();

        Joueur Alice1 = new Joueur("Alice",true);
        Joueur Bob1 = new Joueur("Bob");
        Joueur Charles1 = new Joueur("Charles");
        partie.ajouter(Alice1);
        partie.ajouter(Bob1);
        partie.ajouter(Charles1);
        partie.initialiser(jeuDeCarte);
        partie.distributionCarte(3);
        partie.initialiserRegle();


        try {
            Alice1.poser(Alice1.getJeu().get(0));
        } catch (Exception e) {

        }

        try {
            Alice1.finDeTour();
        } catch (Exception e) {

        }
        try {
            Bob1.poser(Bob1.getJeu().get(0));
        } catch (Exception e) {

        }
        try {
            Bob1.finDeTour();
        } catch (Exception e) {

        }
        try {
            Charles1.poser(Charles1.getJeu().get(2));
        } catch (Exception e) {
            if(Charles1.getNombreCarte()!=2) {
                System.out.println("pbm charles1 n'a pas 2cartes");
                cpt += 1;
            }
        }
        try {
            Charles1.poser(Charles1.getJeu().get(1));
            cpt++;
        } catch (Exception e) {
            if(Charles1.getNombreCarte()!=2) {
                System.out.println("pbm charles1 n'a pas 2cartes");cpt+=1;
            }
            CarteSimple bleu7 = new CarteSimple("Bleu",7);
            if(!Charles1.getCarteDuJeu(1).equals(bleu7)) {
                System.out.println("pbm charles1 n'a pas 7 bleu");
                cpt += 1;
            }
        }
        try {
            Charles1.finDeTour();
        } catch (Exception e) {
        }


        partie.reset();

        Joueur Alice2 = new Joueur("Alice",true);
        Joueur Bob2 = new Joueur("Bob");
        Joueur Charles2 = new Joueur("Charles");
        partie.ajouter(Alice2);
        partie.ajouter(Bob2);
        partie.ajouter(Charles2);
        partie.setPremierJoueur(Alice2);

        partie.initialiser(jeuDeCarte);
        partie.distributionCarte(3);
        partie.initialiserRegle();

//        System.out.println(Alice2);
//        System.out.println(Bob2);
//        System.out.println(Charles2);
//        System.out.println("Tas : "+partie2.getTas());

        try {
            Alice2.finDeTour();
            cpt++;
        } catch (Exception e) {
            if (Alice2.getNombreCarte()!=3) {
                System.out.println("pbm Alice2 n'a pas 3 cartes");
                cpt += 1;
            }
        }

        partie.reset();

        Joueur Alice3 = new Joueur("Alice",true);
        Joueur Bob3 = new Joueur("Bob");
        Joueur Charles3 = new Joueur("Charles");
        partie.ajouter(Alice3);
        partie.ajouter(Bob3);
        partie.ajouter(Charles3);
        partie.setPremierJoueur(Alice3);

        partie.initialiser(jeuDeCarte);
        partie.distributionCarte(3);
        partie.initialiserRegle();

//        System.out.println(Alice3);
//        System.out.println(Bob3);
//        System.out.println(Charles3);
//        System.out.println("Tas : "+partie3.getTas());

        try {
            Alice3.poser(Alice3.getJeu().get(0));
        } catch (Exception e) {

        }
        try {
            Alice3.pioche();
            cpt++;
        } catch (Exception e) {
            {
                if (Alice3.getNombreCarte() != 2) {
                    System.out.println("pbm Alice3 n'a pas 2 cartes");
                    cpt += 1;
                }
            }
        }
        CarteSimple jaune6 = new CarteSimple("Jaune",6);
        if(!partie.getCartePioche(0).equals(jaune6)) {
            System.out.println("pbm carte pioche pas jaune 6");
            cpt += 1;
        }
//        System.out.println(partie3.getCartePioche(0));

        if(cpt==0){
            System.out.println("Test coup illegaux carte simple ok");
        }else{
            System.out.println(cpt+" erreur(s) dans test coup illegaux carte simple");
        }


    }
}
