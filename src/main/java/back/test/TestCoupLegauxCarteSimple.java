package back.test;


import back.carte.CarteSimple;
import back.joueur.Joueur;
import back.parsage.JeuDeCarte;
import back.partie.Partie;

/**
 * Classe testant les coups legaux avec une carte simple.
 */
public class TestCoupLegauxCarteSimple {
    public final static void testCoupLegauxCarteSimple() {
        Partie partie = Partie.getInstance();
        JeuDeCarte jeuDeCarte = new JeuDeCarte("jeux_test/JeuTestCarteSimple.csv");

        partie.initialiserRegle();

        partie.initialiser(jeuDeCarte);
        Joueur Alice = new Joueur("Alice");
        Joueur Bob = new Joueur("Bob");
        Joueur Charles = new Joueur("Charles");
        partie.ajouter(Alice);
        partie.ajouter(Bob);
        partie.ajouter(Charles);
        partie.setPremierJoueur(Alice);

        partie.distributionCarte(3);

//        System.out.println(Alice);
//        System.out.println(Bob);
//        System.out.println(Charles);
//        System.out.println(partie.getTas());

        int cpt=0;

        if(!Alice.isTourDeJeu()){
            System.out.println("pbm alice pas joueur courant");cpt+=1;
        }
        if(Alice.getNombreCarte()!=3){
            System.out.println("pbm alice n'a pas 3 cartes");cpt+=1;
        }
        try {
            Alice.poser(Alice.getJeu().get(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Alice.finDeTour();
        } catch (Exception e) {

        }
        if(Alice.getNombreCarte()!=2){
            System.out.println("pbm alice n'a pas 2 cartes");cpt+=1;
        }
        CarteSimple jaune6 = new CarteSimple("Jaune",6);
        CarteSimple rouge1 = new CarteSimple("Rouge",1);
        if(!Alice.getCarteDuJeu(0).equals(jaune6) || !Alice.getCarteDuJeu(1).equals(rouge1)){
            System.out.println("pbm alice pas 6 jaune et 1 rouge");
            cpt+=1;
        }
//        System.out.println(Alice);
//        System.out.println(partie.getTas().get(0));
        if(partie.getNombreCarteTas()!=2){
            CarteSimple vert2 = new CarteSimple("vert",2);
            if(partie.getCarteTas().equals(vert2)){
                System.out.println("pbm carte tas pas 2 vert");cpt+=1;
            }
            System.out.println("pbm tas n'a pas 2  cartes");cpt+=1;
        }
        if(!Bob.isTourDeJeu()){
            System.out.println("pbm bob pas joueur courant");cpt+=1;
        }
        if(Bob.getNombreCarte()!=3){
            System.out.println("pbm bob n'a pas 3 cartes");cpt+=1;
        }
        try {
            Bob.poser(Bob.getJeu().get(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Bob.finDeTour();
        } catch (Exception e) {

        }
        if(Bob.getNombreCarte()!=2){
            System.out.println("pbm bob n'a pas 2 cartes");cpt+=1;
        }
        CarteSimple jaune4 = new CarteSimple("Jaune",4);
        CarteSimple rouge9 = new CarteSimple("Rouge",9);
        if(!Bob.getCarteDuJeu(0).equals(jaune4) || !Bob.getCarteDuJeu(1).equals(rouge9)){
            System.out.println("pbm bob pas 4 jaune et 9 rouge");cpt+=1;
        }
//        System.out.println(Bob);
//        System.out.println(partie.getTas().get(0));
        CarteSimple bleu2 = new CarteSimple("Bleu",2);
        if(!partie.getCarteTas().equals(bleu2)){
            System.out.println("pbm carte tas pas 2 bleu");
            cpt+=1;
        }
        if(partie.getNombreCarteTas()!=3){
            System.out.println("pbm tas n'a pas 3  cartes");cpt+=1;
        }
        if(!Charles.isTourDeJeu()){
            System.out.println("pbm Charles pas joueur courant");cpt+=1;
        }

        if(cpt==0){
            System.out.println("Test coup legaux carte simple ok");
        }else{
            System.out.println(cpt+" erreur(s) dans test coup legaux carte simple");
        }
    }
}
