package back.test;

/**
 * Classe contenant l'ensemble des tests du projets.
 */
public class EnsembleTest {
    public static void main(String[] args) throws Exception {
        TestCoupLegauxCarteSimple.testCoupLegauxCarteSimple();
        TestCoupIllegauxCarteSimple.testCoupIllegauxCarteSimple();
        TestPunition.testPunition();
        TestUno.testUno();
        TestCoupPasseTour.testCoupPasseTour();
        TestCoupCartePlus2.testCoupCartePlus2();
        TestCoupChangementSens.testCoupChangementSens();
        TestCoupCarteCouleur.testCoupCarteCouleur();
    }
}
