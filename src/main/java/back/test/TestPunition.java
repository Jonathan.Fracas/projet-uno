package back.test;


import back.carte.CarteSimple;
import back.exception.CoupInterditException;
import back.exception.TourException;
import back.joueur.Joueur;
import back.parsage.JeuDeCarte;
import back.partie.Partie;

/**
 * Classe testant les punitions.
 */
public class TestPunition{
    public final static void testPunition() throws Exception {
        Partie partie = Partie.getInstance();
        partie.reset();
        JeuDeCarte jeuDeCarte = new JeuDeCarte("jeux_test/JeuTestCarteSimple.csv");

        partie.initialiserRegle();

        partie.initialiser(jeuDeCarte);
        Joueur Alice = new Joueur("Alice",true);
        Joueur Bob = new Joueur("Bob");
        Joueur Charles = new Joueur("Charles");
        partie.ajouter(Alice);
        partie.ajouter(Bob);
        partie.ajouter(Charles);

        partie.distributionCarte(3);

        int cpt=0;
//        System.out.println(Alice);
//        System.out.println(Bob);
//        System.out.println(Charles);
//        System.out.println(partie.getTas());

        try {
            Alice.poser(Alice.getJeu().get(1));
            System.out.println("pbm alice pose une carte illegale");
            cpt++;
        }catch (CoupInterditException e){
            e.punirJoueur();
            if(!Bob.isTourDeJeu()) {
                System.out.println("pbm bob pas joueur courant");
                cpt += 1;
            }
            if(Alice.getNombreCarte()!=5) {
                System.out.println("pbm alice n'a pas 5 cartes");
                cpt += 1;
            }
            CarteSimple jaune6 = new CarteSimple("Jaune",6);
            CarteSimple rouge4 = new CarteSimple("Rouge",4);
            if(!Alice.getCarteDuJeu(3).equals(jaune6) || !Alice.getCarteDuJeu(4).equals(rouge4)) {
                System.out.println("pbm alice n'a pas 6 jaune et 4 rouge");
                cpt += 1;
            }
//            System.out.println(Alice);
//            System.out.println(partie.getCartePioche(0));
            CarteSimple vert2 = new CarteSimple("Vert",2);
            if(!partie.getCartePioche(0).equals(vert2)) {
                System.out.println("pbm carte pioche pas 2 vert");
                cpt += 1;
            }
        }


        partie.reset();
        Joueur Alice1 = new Joueur("Alice",true);
        Joueur Bob1 = new Joueur("Bob");
        Joueur Charles1 = new Joueur("Charles");
        partie.ajouter(Alice1);
        partie.ajouter(Bob1);
        partie.ajouter(Charles1);
        partie.initialiser(jeuDeCarte);
        partie.distributionCarte(3);
        partie.initialiserRegle();

//        System.out.println(Alice1);
//        System.out.println(Bob1);
//        System.out.println(Charles1);
//        System.out.println("Tas : "+partie1.getTas());
        if(!Alice1.isTourDeJeu()) {
            System.out.println("pbm alice1 pas joueur courant");
            cpt += 1;
        }

        try {
            Bob1.pioche();
            System.out.println("pbm bob pioche alors que ce n'est pas son tour");
            cpt++;
        }catch (TourException e){
            e.punirJoueur();
            if(!Alice1.isTourDeJeu()) {
                System.out.println("pbm alice1 pas joueur courant");
                cpt += 1;
            }
            if(Bob1.getNombreCarte()!=5) {
                System.out.println("pbm bob1 n'a pas 5  cartes");
                cpt += 1;
            }
            CarteSimple jaune6 = new CarteSimple("Jaune",6);
            CarteSimple rouge4 = new CarteSimple("Rouge",4);
            if(!Bob1.getCarteDuJeu(3).equals(jaune6) || !Bob1.getCarteDuJeu(4).equals(rouge4)) {
                System.out.println("pbm bob1 n'a pas 6 jaune et 4 rouge");
                cpt += 1;
            }
            CarteSimple vert2 = new CarteSimple("Vert",2);
            if(!partie.getCartePioche(0).equals(vert2)) {
                System.out.println("pbm carte pioche pas 2 vert");
                cpt += 1;
            }
//            System.out.println(Bob1);
//            System.out.println(partie.getCartePioche(0));
        }

        if(cpt==0){
            System.out.println("Test carte punition ok");
        }else{
            System.out.println(cpt+" erreur(s) dans test carte punition");
        }
    }
}
