package back.exception;

import back.joueur.Joueur;

/**
 * Classe representant une exception produite suite a une action effectue alors que le joueur n'etait pas
 * le joueur courant.
 */
public class TourException extends Exception{
    //*********************************Champs*****************************************
    /**
     * Le joueur qui a effectue un coup interdit.
     */
    private Joueur joueurPuni;
    /**
     * Le nombre de carte a piocher en guise de punition.
     */
    private int nombreCarteAPiocher;

    //*********************************Constructeurs*****************************************
    /**
     * Constructeur d'une tour exception.
     *
     * @param joueurPuni le joueur puni.
     * @param nombreCarteAPiocher le nombre de carte a piocher.
     */
    public TourException(Joueur joueurPuni, int nombreCarteAPiocher) {
        this.joueurPuni = joueurPuni;
        this.nombreCarteAPiocher = nombreCarteAPiocher;
    }

    //*********************************Getters*****************************************
    public Joueur getJoueurPuni() {
        return joueurPuni;
    }

    public int getNombreCarteAPiocher() {
        return nombreCarteAPiocher;
    }

    //*********************************Méthodes*****************************************
    /**
     * Methode qui puni un joueur qui a lancé une TourException.
     * Ce joueur pioche @nombreCarteAPiocher cartes.
     */
    public void punirJoueur(){
        for (int i = 0; i < getNombreCarteAPiocher(); i++) {
            joueurPuni.piochePunition();
        }
    }
}
