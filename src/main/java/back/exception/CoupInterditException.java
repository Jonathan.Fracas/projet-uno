package back.exception;

import back.joueur.Joueur;
import back.partie.Partie;


/**
 * Classe representant une exception produite suite a un coup interdit.
 */
public class CoupInterditException extends Exception {
    //*********************************Champs*****************************************
    /**
     * Le joueur qui a effectue un coup interdit.
     */
    private Joueur joueurPuni;
    /**
     * Le nombre de carte a piocher en guise de punition.
     */
    private int nombreCarteAPiocher;

    //*********************************Constructeur*****************************************

    /**
     * Constructeur d'une coup interdit exception.
     *
     * @param joueurPuni le joueur puni.
     * @param nombreCarteAPiocher le nombre de carte a piocher.
     */
    public CoupInterditException(Joueur joueurPuni,int nombreCarteAPiocher){
        this.joueurPuni = joueurPuni;
        this.nombreCarteAPiocher = nombreCarteAPiocher;
    }

    //*********************************Getters*****************************************
    public Joueur getJoueurPuni() {
        return joueurPuni;
    }

    public int getNombreCarteAPiocher() {
        return nombreCarteAPiocher;
    }

    //*********************************Champs*****************************************
    /**
     * Methode qui puni un joueur qui a lancé une CoupInterditException.
     * Ce joueur pioche @nombreCarteAPiocher cartes.
     */
    public void punirJoueur()  {
        for (int i = 0; i < getNombreCarteAPiocher(); i++) {
            joueurPuni.piochePunition();
        }
        Partie.getInstance().setJoueurSuivant();
        joueurPuni.finDeTourAutomatique();
    }
}
