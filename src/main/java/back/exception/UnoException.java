package back.exception;

import back.joueur.Joueur;

/**
 * Classe representant une exception produite suite lorsqu'un joueur oublie de dire uno ou dit uno sans avoir
 * les conditions necessaires pour le dire.
 */
public class UnoException extends Exception{
    //*********************************Champs*****************************************
    /**
     * Le joueur qui a effectue un coup interdit.
     */
    private Joueur joueurPuni;
    /**
     * Le nombre de carte a piocher en guise de punition.
     */
    private int nombreCarteAPiocher;

    //*********************************Constructeur****************************************
    /**
     * Constructeur d'une coup uno exception.
     *
     * @param joueurPuni le joueur puni.
     * @param nombreCarteAPiocher le nombre de carte a piocher.
     */
    public UnoException(Joueur joueurPuni, int nombreCarteAPiocher) {
        this.joueurPuni = joueurPuni;
        this.nombreCarteAPiocher = nombreCarteAPiocher;
    }

    //*********************************Getters*****************************************
    public Joueur getJoueurPuni() {
        return joueurPuni;
    }

    public int getNombreCarteAPiocher() {
        return nombreCarteAPiocher;
    }

    //*********************************Méthodes*****************************************
    /**
     * Methode qui puni un joueur qui a lancé une UnoException.
     * Ce joueur pioche @nombreCarteAPiocher cartes.
     */
    public void punirJoueur(){
        for (int i = 0; i < getNombreCarteAPiocher(); i++) {
            joueurPuni.piochePunition();
        }
    }
}
