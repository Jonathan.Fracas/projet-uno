package back.regles;

public class RegleException extends Exception {
    //*********************************Constructeur*****************************************
    /**
     * Constructeur.
     * @param msg message affiche en sortie.
     */
    public RegleException(String msg) {
        super(msg);
    }
}
