package back.regles.reglecartepasser;

import back.carte.Carte;
import back.carte.*;
import back.exception.CoupInterditException;
import back.joueur.Joueur;
import back.partie.Partie;
import back.regles.Regles;

/**
 * Classe correspondant a la pose d'une carte passer sur une carte passer.
 */
public class RegleCartePasserSurCartePasser extends Regles {
    public RegleCartePasserSurCartePasser(Regles suivant) {
        super(suivant);
    }

    /**
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @return vrai si le joueur pose une carte passer sur une carte passer.
     */
    @Override
    public boolean saitRegler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        return carteJoueur.getClass()== CartePasser.class && carteTas.getClass()== CartePasser.class;
    }

    /**
     * Le joueur peut poser une carte passer sur une carte passer.
     * Si le coup est legal, le joueur suivant passe son tour et le joueur courant devient le joueur qui vient apres lui.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     */
    @Override
    public void regler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        Partie.getInstance().poser(carteJoueur);
        setJoueurSuivantCartePasser();
    }
}
