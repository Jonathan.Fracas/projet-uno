package back.regles.reglecartepasser;

import back.carte.Carte;
import back.carte.*;
import back.exception.CoupInterditException;
import back.joueur.Joueur;
import back.partie.Partie;
import back.regles.Regles;

/**
 * Classe correspondant a la pose d'une carte passer sur une carte couleur.
 */
public class RegleCartePasserSurCarteCouleur extends Regles {
    public RegleCartePasserSurCarteCouleur(Regles suivant) {
        super(suivant);
    }

    /**
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @return vrai si le joueur pose une carte passer sur une carte couleur.
     */
    @Override
    public boolean saitRegler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        return carteJoueur.getClass()== CartePasser.class && carteTas.getClass()== CarteCouleur.class;
    }

    /**
     * Le joueur peut poser une carte passer sur une carte couleur si la couleur
     * des deux cartes est identiques.
     * Si le coup est legal, le joueur suivant passe son tour et le joueur courant devient le joueur qui vient apres lui.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @throws CoupInterditException est lancee si le coup est interdit.
     */
    @Override
    public void regler(Joueur joueur, Carte carteJoueur, Carte carteTas) throws CoupInterditException {
        CartePasser cartePasser = (CartePasser) carteJoueur;
        if(cartePasser.equalsCouleur(carteTas) || carteTas.getCouleur() == null){
            Partie.getInstance().poser(carteJoueur);
            carteTas.resetCouleurJoker();
            setJoueurSuivantCartePasser();
        }
        else{
            Partie.getInstance().setJoueurSuivant(Partie.getInstance().getJoueurSuivant(Partie.getInstance().getJoueurCourant()));
            throw new CoupInterditException(joueur,Partie.NOMBRE_CARTE_PIOCHER_COUP_INTERDIT);
        }
    }
}
