package back.regles.reglecartepasser;

import back.regles.Regles;
import back.carte.Carte;
import back.carte.*;
import back.exception.CoupInterditException;
import back.joueur.Joueur;
import back.partie.Partie;


/**
 * Classe correspondant a la pose d'une carte passer sur une carte simple.
 */
public class RegleCartePasserSurCarteSimple extends Regles {
    public RegleCartePasserSurCarteSimple(Regles suivant) {
        super(suivant);
    }

    /**
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @return vrai si le joueur pose une carte passer sur une carte simple.
     */
    @Override
    public boolean saitRegler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        return carteJoueur.getClass()== CartePasser.class && carteTas.getClass()== CarteSimple.class;
    }

    /**
     * Le joueur peut poser une carte passer sur une carte simple si la couleur
     * des deux cartes est identiques.
     * Si le coup est legal, le joueur suivant passe son tour et le joueur courant devient le joueur qui vient apres lui.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @throws CoupInterditException est lancee si le coup est interdit.
     */
    @Override
    public void regler(Joueur joueur, Carte carteJoueur, Carte carteTas) throws CoupInterditException {
        CartePasser cartePasser = (CartePasser) carteJoueur;
        if(cartePasser.equalsCouleur(carteTas)){
            Partie.getInstance().poser(carteJoueur);
            setJoueurSuivantCartePasser();
        }
        else{
            Partie.getInstance().setJoueurSuivant(Partie.getInstance().getJoueurSuivant(Partie.getInstance().getJoueurCourant()));
            throw new CoupInterditException(joueur,Partie.NOMBRE_CARTE_PIOCHER_COUP_INTERDIT);
        }
    }
}
