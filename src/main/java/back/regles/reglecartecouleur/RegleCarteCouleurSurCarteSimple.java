package back.regles.reglecartecouleur;

import back.carte.Carte;
import back.carte.*;
import back.exception.CoupInterditException;
import back.joueur.Joueur;
import back.partie.Partie;
import back.regles.Regles;

/**
 * Classe correspondant a la pose d'une carte couleur sur une carte simple.
 */
public class RegleCarteCouleurSurCarteSimple extends Regles {
    public RegleCarteCouleurSurCarteSimple(Regles suivant) {
        super(suivant);
    }

    /**
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @return vrai si le joueur pose une carte couleur sur une carte simple.
     */
    @Override
    public boolean saitRegler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        return carteJoueur.getClass()== CarteCouleur.class && carteTas.getClass()== CarteSimple.class;
    }

    /**
     * Le joueur peut poser une carte couleur sur une carte simple.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     */
    @Override
    public void regler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        Partie.getInstance().poser(carteJoueur);
        setJoueurSuivantCarteSimpleCarteCouleurCarteChangementDeSens();
    }
}
