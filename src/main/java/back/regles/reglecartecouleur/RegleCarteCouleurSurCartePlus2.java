package back.regles.reglecartecouleur;

import back.carte.Carte;
import back.carte.*;
import back.exception.CoupInterditException;
import back.joueur.Joueur;
import back.partie.Partie;
import back.regles.Regles;

/**
 * Classe correspondant a la pose d'une carte couleur sur une carte plus 2.
 */
public class RegleCarteCouleurSurCartePlus2 extends Regles {
    public RegleCarteCouleurSurCartePlus2(Regles suivant) {
        super(suivant);
    }

    /**
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @return vrai si le joueur pose une carte couleur sur une carte plus 2.
     */
    @Override
    public boolean saitRegler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        return carteJoueur.getClass()== CarteCouleur.class && carteTas.getClass()== CartePlus2.class;
    }

    /**
     * Le joueur peut poser une carte couleur sur une carte plus 2.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     */
    @Override
    public void regler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        Partie.getInstance().poser(carteJoueur);
        setJoueurSuivantCarteSimpleCarteCouleurCarteChangementDeSens();
    }


}
