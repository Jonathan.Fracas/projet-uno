package back.regles.reglecarteplus2;

import back.carte.Carte;
import back.carte.*;
import back.exception.CoupInterditException;
import back.joueur.Joueur;
import back.partie.Partie;
import back.regles.Regles;

/**
 * Classe correspondant a la pose d'une carte plus 2 sur une carte plus 2.
 */
public class RegleCartePlus2SurCartePlus2 extends Regles {
    public RegleCartePlus2SurCartePlus2(Regles suivant) {
        super(suivant);
    }

    /**
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @return vrai si le joueur pose une carte plus 2 sur une carte plus 2.
     */
    @Override
    public boolean saitRegler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        return carteJoueur.getClass()== CartePlus2.class && carteTas.getClass()==CartePlus2.class;
    }

    /**
     * Le joueur peut poser une carte plus 2 sur une carte plus 2.
     * Si le coup est legal, le joueur suivant aura le choix entre encaisser l'attaque ou poser une carte
     * plus 2 ou plus 4.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     */
    @Override
    public void regler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        Partie.getInstance().getJoueurSuivant(joueur).setCarteAPiocher(joueur.getCarteAPiocher());
        joueur.resetCarteAPiocher();
        Partie.getInstance().poser(carteJoueur);
        setJoueurSuivantCartePlus2();
    }
}