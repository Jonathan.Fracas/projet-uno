package back.regles.reglecarteplus2;

import back.carte.Carte;
import back.carte.*;
import back.exception.CoupInterditException;
import back.joueur.Joueur;
import back.partie.Partie;
import back.regles.Regles;

/**
 * Classe correspondant a la pose d'une carte plus 2 sur une carte couleur.
 */
public class RegleCartePlus2SurCarteCouleur extends Regles {
    public RegleCartePlus2SurCarteCouleur(Regles suivant) {
        super(suivant);
    }

    /**
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @return vrai si le joueur pose une carte plus 2 sur une carte couleur.
     */
    @Override
    public boolean saitRegler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        return carteJoueur.getClass()== CartePlus2.class && carteTas.getClass()== CarteCouleur.class;
    }

    /**
     * Le joueur peut poser une carte plus 2 sur une carte couleur si la couleur
     * des deux cartes est identiques.
     * Si le coup est legal, le joueur suivant aura le choix entre encaisser l'attaque ou poser une carte
     * plus 2 ou plus 4.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @throws CoupInterditException est lancee si le coup est interdit.
     */
    @Override
    public void regler(Joueur joueur, Carte carteJoueur, Carte carteTas) throws CoupInterditException {
        CartePlus2 cartePlus2 = (CartePlus2) carteJoueur;
        if(cartePlus2.equalsCouleur(carteTas) || carteTas.getCouleur() == null){
            Partie.getInstance().poser(carteJoueur);
            carteTas.resetCouleurJoker();
            setJoueurSuivantCartePlus2();
        }
        else{
            Partie.getInstance().setJoueurSuivant(Partie.getInstance().getJoueurSuivant(Partie.getInstance().getJoueurCourant()));
            throw new CoupInterditException(joueur,Partie.NOMBRE_CARTE_PIOCHER_COUP_INTERDIT);
        }
    }
}
