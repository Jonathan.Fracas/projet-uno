package back.regles.reglecartesimple;

import back.carte.Carte;
import back.carte.*;
import back.exception.CoupInterditException;
import back.joueur.Joueur;
import back.partie.Partie;
import back.regles.Regles;

/**
 * Classe correspondant a la pose d'une simple sur une carte passer.
 */
public class RegleCarteSimpleSurCartePasser extends Regles {
    public RegleCarteSimpleSurCartePasser(Regles suivant) {
        super(suivant);
    }

    /**
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @return vrai si le joueur pose une carte simple sur une carte passer.
     */
    @Override
    public boolean saitRegler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        return carteJoueur.getClass()== CarteSimple.class && carteTas.getClass()== CartePasser.class;
    }

    /**
     * Le joueur peut poser une carte simple sur une carte passer si la couleur
     * des deux cartes est identiques.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @throws CoupInterditException est lancee si le coup est interdit.
     */
    @Override
    public void regler(Joueur joueur, Carte carteJoueur, Carte carteTas) throws CoupInterditException {
        CarteSimple carteSimple = (CarteSimple) carteJoueur;
        setJoueurSuivantCarteSimpleCarteCouleurCarteChangementDeSens();
        if(carteSimple.equalsCouleur(carteTas)){
            Partie.getInstance().poser(carteJoueur);
        }
        else{
            throw new CoupInterditException(joueur,Partie.NOMBRE_CARTE_PIOCHER_COUP_INTERDIT);
        }
    }

}
