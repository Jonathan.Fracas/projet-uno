package back.regles.reglecartechangementdesens;


import back.carte.Carte;
import back.carte.CarteChangementDeSens;
import back.carte.CarteCouleur;
import back.carte.CarteSimple;
import back.exception.CoupInterditException;
import back.joueur.Joueur;
import back.partie.Partie;
import back.regles.Regles;

/**
 * Classe correspondant a la pose d'une carte changement de sens sur une carte couleur.
 */
public class RegleCarteChangementDeSensSurCarteCouleur extends Regles {
    public RegleCarteChangementDeSensSurCarteCouleur(Regles suivant) {
        super(suivant);
    }

    /**
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @return vrai si le joueur pose une carte changement de sens sur une carte couleur.
     */
    @Override
    public boolean saitRegler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        return carteJoueur.getClass()== CarteChangementDeSens.class && carteTas.getClass()== CarteCouleur.class;
    }

    /**
     * Le joueur peut poser une carte changement de sens sur une carte couleur si la couleur
     * des deux cartes est identiques.
     * Si le coup est legal, la partie change de sens.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @throws CoupInterditException est lancee si le coup est interdit.
     */
    @Override
    public void regler(Joueur joueur, Carte carteJoueur, Carte carteTas) throws CoupInterditException {
        CarteChangementDeSens carteChangementDeSens = (CarteChangementDeSens) carteJoueur;
        if(carteChangementDeSens.equalsCouleur(carteTas) || carteTas.getCouleur() == null){
            Partie.getInstance().poser(carteJoueur);
            carteTas.resetCouleurJoker();
            Partie.getInstance().changementSens();
            setJoueurSuivantCarteSimpleCarteCouleurCarteChangementDeSens();
        }
        else{
            setJoueurSuivantCarteSimpleCarteCouleurCarteChangementDeSens();
            throw new CoupInterditException(joueur,Partie.NOMBRE_CARTE_PIOCHER_COUP_INTERDIT);
        }
    }
}
