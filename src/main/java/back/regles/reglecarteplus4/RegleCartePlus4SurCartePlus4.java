package back.regles.reglecarteplus4;

import back.carte.Carte;
import back.carte.*;
import back.exception.CoupInterditException;
import back.joueur.Joueur;
import back.partie.Partie;
import back.regles.Regles;
/**
 * Classe correspondant a la pose d'une carte plus 4 sur une carte plus 4.
 */
public class RegleCartePlus4SurCartePlus4 extends Regles {
    public RegleCartePlus4SurCartePlus4(Regles suivant) {
        super(suivant);
    }

    /**
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @return vrai si le joueur pose une carte plus 4 sur une carte plus 4.
     */
    @Override
    public boolean saitRegler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        return carteJoueur.getClass()== CartePlus4.class && carteTas.getClass()==CartePlus4.class;
    }

    /**
     * Le joueur peut poser une carte plus 4 sur une carte plus 4.
     * Le joueur suivant pioche 4 cartes et passe son tour.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     */
    @Override
    public void regler(Joueur joueur, Carte carteJoueur, Carte carteTas) {
        Partie.getInstance().getJoueurSuivant(joueur).setCarteAPiocher(joueur.getCarteAPiocher());
        joueur.resetCarteAPiocher();
        Partie.getInstance().poser(carteJoueur);
        carteTas.resetCouleurJoker();
        setJoueurSuivantCartePlus4();
    }
}
