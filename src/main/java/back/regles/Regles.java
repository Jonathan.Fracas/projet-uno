package back.regles;


import back.carte.Carte;
import back.exception.CoupInterditException;
import back.joueur.Joueur;
import back.partie.Partie;
import ihm.javafx.Uno;

/**
 * Chaine de responsabilite representant les regles du jeu.
 */
public abstract class Regles {
    //*********************************Champ*****************************************
    private Regles suivant = null;


    //*********************************Constructeur*****************************************
    public Regles(Regles suivant) {
        this.suivant = suivant;
    }


    //*********************************Getters*****************************************
    private Regles getSuivant() {
        return suivant;
    }

    private boolean aUnSuivant() {
        return suivant != null;
    }

    //*********************************Setters*****************************************
    public void setJoueurSuivantCarteSimpleCarteCouleurCarteChangementDeSens(){
        Partie.getInstance().setJoueurSuivant(Partie.getInstance().getJoueurSuivant(Partie.getInstance().getJoueurCourant()));
    }

    public void setJoueurSuivantCartePlus4(){
        Joueur joueurSuivant = Partie.getInstance().getJoueurSuivant(Partie.getInstance().getJoueurCourant());
        Partie.getInstance().setJoueurSuivant(joueurSuivant);
        joueurSuivant.setCarteAPiocher(joueurSuivant.getCarteAPiocher()+4);
    }

    public void setJoueurSuivantCartePlus2(){
        Joueur joueurSuivant = Partie.getInstance().getJoueurSuivant(Partie.getInstance().getJoueurCourant());
        Partie.getInstance().setJoueurSuivant(joueurSuivant);
        joueurSuivant.setCarteAPiocher(joueurSuivant.getCarteAPiocher()+2);
    }

    public void setJoueurSuivantCartePasser(){
        Partie.getInstance().setJoueurSuivant(Partie.getInstance().getJoueurSuivant(Partie.getInstance().getJoueurSuivant(Partie.getInstance().getJoueurCourant())));
    }

    //*********************************Méthodes*****************************************
    /**
     * La fonction traiter() parcours la liste à la recherche d'un maillon qui sait comment parser
     * la ligne. Dans ce cas la ligne est parsée et la recherche s'arrête.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @throws RegleException est lancee s'il n'existe pas de regles pour ce coup.
     * @throws CoupInterditException est lancee si le coup est illegal.
     */
    public void traiter(Joueur joueur, Carte carteJoueur, Carte carteTas) throws RegleException, CoupInterditException {
        if(saitRegler(joueur,carteJoueur,carteTas)){
            try {
                regler(joueur,carteJoueur,carteTas);
            } catch (CoupInterditException e) {
                throw e;
            }
        }
        else if (aUnSuivant()){
            getSuivant().traiter(joueur,carteJoueur, carteTas);
        }
        else {
            throw new RegleException("Pas de regles pour cette carte"+carteJoueur+"poser sur"+carteTas);
        }
    }

    /**
     * Renvoie true si la regle peut traiter la pose de la carte du joueur sur
     * le tas, false sinon.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posee.
     * @param carteTas la carte du tas.
     * @return true si on peut regler cette pose de carte, false sinon.
     */
    public abstract boolean saitRegler(Joueur joueur, Carte carteJoueur, Carte carteTas);

    /**
     * Regarde si la carte poser respecte les regles du jeu et la pose si c'est le cas,
     * renvoie une CoupInterditException sinon.
     *
     * @param joueur le joueur qui pose une carte.
     * @param carteJoueur la carte posée.
     * @param carteTas la carte du tas.
     * @throws CoupInterditException est lancee si le coup est illegal.
     */
    public abstract void regler(Joueur joueur, Carte carteJoueur, Carte carteTas) throws CoupInterditException;

}
