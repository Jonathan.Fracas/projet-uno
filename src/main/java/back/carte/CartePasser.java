package back.carte;

/**
 * Classe d'une carte passer.
 */
public class CartePasser extends Carte{
    //*********************************Constructeur*****************************************
    /**
     * Constructeur d'une carte passer.
     *
     * Une carte passer est caractérisé par une couleur.
     * @param couleur couleur de la carte.
     */
    public CartePasser(String couleur) {
        super(couleur);
    }

    //*********************************Getters*****************************************
    @Override
    public String getImageURL() {
        return "images/carte_Passe_"+getCouleur()+".png";
    }

    //*********************************Méthodes*****************************************
    @Override
    public String toString() {
        return "Carte Passer "+
                super.toString();
    }
}
