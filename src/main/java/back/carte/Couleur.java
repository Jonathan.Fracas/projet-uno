package back.carte;

/**
 * Classe (enum) contenant les couleurs des cartes du jeu.
 */
public enum Couleur {
    ROUGE("Rouge"), JAUNE("Jaune"), VERT("Vert"), BLEU("Bleu");

    private String nomCouleur ;

    private Couleur(String nomCouleur) {
        this.nomCouleur = nomCouleur ;
    }

    public String getNomCouleur() {
        return  this.nomCouleur ;
    }
}
