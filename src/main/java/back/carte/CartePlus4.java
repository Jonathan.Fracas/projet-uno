package back.carte;

/**
 * Classe d'une carte plus 4.
 */
public class CartePlus4 extends Carte{
    //*********************************Constructeur*****************************************
    /**
     * Constructeur d'une carte plus 4.
     *
     * La carte plus 4 est une carte joker.
     * On considere comme null  la couleur d'une carte plus 4,
     * la carte plus 4 est une carte punition
     */
    public CartePlus4() {
        super(null,true,true);
    }

    //*********************************Getters*****************************************
    @Override
    public String getImageURL() {
        return "images/carte_Plus4.png";
    }

    //*********************************Méthodes*****************************************
    @Override
    public String toString() {
        return "Carte Plus 4";
    }
}
