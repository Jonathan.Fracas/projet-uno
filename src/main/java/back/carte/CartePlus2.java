package back.carte;

/**
 * Classe d'une carte plus 2.
 */
public class CartePlus2 extends Carte{
    //*********************************Constructeur*****************************************
    /**
     * Constructeur d'une carte plus 2.
     *
     * La carte plus 2 est caractérisee par une couleur, et est une carte punition.
     * @param couleur la couleur de la carte.
     * @param punition vrai.
     */
    public CartePlus2(String couleur,boolean punition) {
        super(couleur, punition);
    }

    //*********************************Getters*****************************************
    @Override
    public String getImageURL() {
        return "images/carte_Plus2_"+getCouleur()+".png";
    }

    //*********************************Méthodes*****************************************
    @Override
    public String toString() {
        return "Carte Plus 2 "+
                super.toString();
    }
}
