package back.carte;

/**
 * Classe d'une carte couleur.
 */
public class CarteCouleur extends Carte{
    //*********************************Constructeur*****************************************
    /**
     * Constructeur d'une carte couleur.
     *
     * La carte couleur est une carte joker.
     * On considere comme null  la couleur d'une carte couleur,
     * la carte couleur n'est pas une carte punition
     */
    public CarteCouleur() {
        super(null,false,true);
    }

    //*********************************Getters*****************************************
    @Override
    public String getImageURL() {
        return "images/carte_Change_Couleur.png";
    }

    //*********************************Méthodes*****************************************
    @Override
    public String toString() {
        return "Carte Couleur";
    }

}
