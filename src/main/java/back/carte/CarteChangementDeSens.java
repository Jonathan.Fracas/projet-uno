package back.carte;

/**
 * Classe d'une carte changement de sens.
 */
public class CarteChangementDeSens extends Carte{
    //*********************************Constructeur*****************************************
    /**
     * Constructeur d'une carte changement de sens.
     *
     * Une carte changement de sens est caractérisé par une couleur.
     * @param couleur couleur de la carte.
     */
    public CarteChangementDeSens(String couleur) {
        super(couleur);
    }

    //*********************************Getters*****************************************
    @Override
    public String getImageURL() {
        return "images/carte_Change_"+getCouleur()+".png";
    }

    //*********************************Méthodes*****************************************
    @Override
    public String toString() {
        return "Carte Changement De Sens "+
                super.toString();
    }
}
