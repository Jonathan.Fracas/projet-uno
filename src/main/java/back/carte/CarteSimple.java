package back.carte;

/**
 * Classe d'une carte simple.
 */
public class CarteSimple extends Carte{
    //*********************************Champs*****************************************
    /**
     * Valeur de la carte simple.
     */
    private int valeur;

    //*********************************Constantes*****************************************
    /**
     * Valeur minimale d'une carte simple.
     */
    public static final int VALEUR_MIN = 0;
    /**
     * Valeur maximale d'une carte simple.
     */
    public static final int VALEUR_MAX = 9;

    //*********************************Constructeurs*****************************************
    /**
     * Constructeur d'une carte simple.
     *
     * Une carte simple est caractérisé par une couleur et une valeur.
     * @param couleur la couleur de la carte.
     * @param valeur la valeur de la carte.
     */
    public CarteSimple(String couleur,int valeur) {
        super(couleur);
        setValeur(valeur);
    }

    //*********************************Setters*****************************************
    private void setValeur(int valeur){
        if(valeur < VALEUR_MIN || valeur > VALEUR_MAX){
            throw new IllegalArgumentException("valeur non valide");
        }
        this.valeur = valeur;
    }

    //*********************************Getters*****************************************
    public int getValeur() {
        return valeur;
    }

    @Override
    public String getImageURL() {
        return "images/carte_"+valeur+"_"+getCouleur()+".png";
    }
    //*********************************Méthodes*****************************************
    public boolean equalsValeur(CarteSimple carteSimple){
        return this.getValeur() == carteSimple.getValeur();
    }



    @Override
    public String toString() {
        return "Carte Simple "+ valeur + " "+
                super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarteSimple that = (CarteSimple) o;
        return valeur == that.valeur  && super.equals(o);
    }
}
