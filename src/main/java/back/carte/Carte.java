package back.carte;


import java.util.Objects;

/**
 * Classe abstraite, represente une carte du jeu.
 * */
public abstract class Carte {
    //*********************************Champs*****************************************
    /**
     * Couleur de la carte.
     */
    private String couleur;
    /**
     * Vrai si la carte est une carte punition, faux sinon.
     */
    private boolean punition = false;
    /**
     * Vrai si la carte est une carte joker, faux sinon.
     */
    private boolean joker = false;

    //*********************************Constructeurs*****************************************

    /**
     * Constructeur par défaut.
     */
    public Carte() {
    }

    /**
     * Constructeur d'une carte avec couleur.
     * @param couleur la couleur de la carte.
     */
    public Carte(String couleur) {
        setCouleur(couleur);
    }

    /**
     * Constructeur d'une carte punition avec couleur.
     * @param couleur la couleur de la carte.
     * @param punition carte punition.
     */
    public Carte(String couleur, boolean punition) {
        setCouleur(couleur);
        this.punition = punition;
    }

    /**
     * Constructeur d'une carte joker.
     * @param couleur null à la construction.
     * @param punition vrai si carte punition faux sinon.
     * @param joker carte joker.
     */
    public Carte(String couleur, boolean punition, boolean joker) {
        setCouleur(couleur);
        this.punition = punition;
        this.joker = joker;
    }


    //*********************************Setters*****************************************
    private void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    private void setImageURL(String imageURL) {
        if (imageURL == null || imageURL.trim().equals("")) {
            throw new IllegalArgumentException("url null ou vide");
        }
    }

    public void setCouleurJoker(String couleur){
        if(getCouleur() != null){
            throw new IllegalArgumentException("tentative de changement de couleur d'une carte non joker");
        }
        this.couleur = couleur;
    }

    public void resetCouleurJoker(){
        if(joker){
            setCouleur(null);
        }
    }

    //*********************************Getters*****************************************
    public String getCouleur() {
        return couleur;
    }

    public boolean isPunition() {
        return punition;
    }

    public boolean isJoker(){
        return joker;
    }

    public abstract String getImageURL();

    //*********************************Méthodes*****************************************
    public boolean equalsCouleur(Carte carte){
        return this.getCouleur().equals(carte.getCouleur());
    }

    @Override
    public String toString() {
        return couleur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Carte carte = (Carte) o;
        return Objects.equals(couleur, carte.couleur);
    }
}
