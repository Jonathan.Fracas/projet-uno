package back.partie;


import back.carte.*;
import back.joueur.Joueur;
import back.parsage.JeuDeCarte;
import back.parsage.parser.*;
import back.regles.Regles;
import back.regles.reglecartechangementdesens.*;
import back.regles.reglecartecouleur.*;
import back.regles.reglecartepasser.*;
import back.regles.reglecarteplus2.*;
import back.regles.reglecarteplus4.*;
import back.regles.reglecartesimple.*;
import javafx.scene.canvas.Canvas;

import java.util.ArrayList;

/**
 * Classe (singleton) representant une partie.
 * Une partie contient :
 * - une liste de joueurs.
 * - une pioche.
 * - un tas.
 * Par defaut, une partie se deroule dans le sens horaire.
 */
public final class Partie {
    //*********************************Champs*****************************************
    /**
     * Initialisation du singleton
     */
    private static final Partie INSTANCE = new Partie();
    /**
     * la pioche de la partie.
     */
    private ArrayList<Carte> pioche = new ArrayList<Carte>();
    /**
     * le tas de la partie.
     */
    private ArrayList<Carte> tas = new ArrayList<Carte>();
    /**
     * les joueurs de la partie.
     */
    private ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
    /**
     * Le sens de la partie.
     */
    private boolean sensHoraire = true;
    /**
     * Les regles du jeu.
     */
    private Regles regles;
    /**
     * Le prochain joueur a joue.
     */
    private Joueur joueurSuivant;

    //*********************************Constantes*****************************************
    /**
     * nombre de carte a piocher suite a un coup interdit.
     */
    public static final int NOMBRE_CARTE_PIOCHER_COUP_INTERDIT = 2;
    /**
     * nombre de carte a piocher suite a non respect de son tour de jeu.
     */
    public static final int NOMBRE_CARTE_PIOCHER_TOUR_INTERDIT = 2;
    /**
     * nombre de carte a piocher suite a un contre uno.
     */
    public static final int NOMBRE_CARTE_PIOCHER_CONTRE_UNO = 2;

    //*********************************Constructeurs*****************************************
    /**
     * Constructeur d'une partie avec la pioche, les joueurs et les regles.
     * @param pioche les cartes du jeu.
     * @param joueurs la joueurs.
     * @param reglesDuJeu les regles de la partie.
     */
    public Partie(ArrayList<Carte> pioche, ArrayList<Joueur> joueurs, Regles reglesDuJeu) {
        setPioche(pioche);
        setJoueurs(joueurs);
        setRegles(reglesDuJeu);
    }

    /**
     * Constructeur par defaut d'une partie.
     */
    public Partie() {
    }

    //*********************************Setters*****************************************
    public void setPioche(ArrayList<Carte> pioche) {
        if (pioche == null) {
            throw new IllegalArgumentException("La pioche est null");
        }
        this.pioche = pioche;
    }

    public void setJoueurs(ArrayList<Joueur> joueurs) {
        if (joueurs == null) {
            throw new IllegalArgumentException("La liste de joueurs est null");
        }
        this.joueurs = joueurs;
    }

    public void setRegles(Regles regles) {
        if (regles == null) {
            throw new IllegalArgumentException("regles du jeu null");
        }
        this.regles = regles;
    }

    private void setSensHoraire(boolean sensHoraire) {
        this.sensHoraire = sensHoraire;
    }

    public void changementSens(){
        setSensHoraire(!isSensHoraire());
    }

    public void setJoueurSuivant(){
        getJoueurSuivant().setTourDeJeu(true);
    }

    public void setJoueurSuivant(Joueur joueurSuivant){
        if (joueurSuivant == null) {
            throw new IllegalArgumentException("joueur suivant null");
        }
        this.joueurSuivant=joueurSuivant;
    }

    public void setPremierJoueur(Joueur joueur) {
        joueur.setTourDeJeu(true);
        setJoueurSuivant(getJoueurSuivant(joueur));
    }

    //*********************************Getters*****************************************
    public static Partie getInstance() {
        return INSTANCE;
    }

    public boolean isSensHoraire() {
        return sensHoraire;
    }

    public Regles getRegles() {
        return regles;
    }

    public ArrayList<Carte> getPioche() {
        return pioche;
    }

    public ArrayList<Carte> getTas() {
        return tas;
    }

    public Joueur getJoueurSUivant(){
        return joueurSuivant;
    }

    public ArrayList<Joueur> getJoueurs() {
        return joueurs;
    }

    public int getNombreJoueur(){
        return joueurs.size();
    }

    public Joueur getJoueur(int indice){
        if(indice < 0 || indice > getNombreJoueur()-1){
            throw new IllegalArgumentException("indice non valide");
        }
        return joueurs.get(indice);
    }

    public int getNombreCartePioche(){
        return pioche.size();
    }

    public int getNombreCarteTas(){
        return tas.size();
    }

    /**
     * Renvoie la carte au sommet du tas.
     * @return Carte la carte du sommet du tas.
     */
    public Carte getCarteTas(){
        if(getNombreCarteTas()==0){
            throw new IllegalArgumentException("tas vide");
        }
        return tas.get(0);
    }

    public Carte getCartePioche(int indice){
        if(indice < 0 || indice > getNombreCartePioche()-1){
            throw new IllegalArgumentException("indice non valide");
        }
        return pioche.get(indice);
    }

    public Carte getCarteTas(int indice){
        if(indice < 0 || indice > getNombreCarteTas()-1){
            throw new IllegalArgumentException("indice non valide");
        }
        return tas.get(indice);
    }

    public Joueur getJoueurCourant(){
        for (Joueur joueur: joueurs
             ) {
            if (joueur.isTourDeJeu()){
                return joueur;
            }
        }
        return null;
    }

    /**
     * Selon le sens de la partie, on renvoie le joueur suivant le joueur courant.
     * @return Joueur : Le joueur suivant.
     */
    public Joueur getJoueurSuivant(Joueur joueur){
        if(isSensHoraire()){
            if(getJoueurs().indexOf(joueur) == getNombreJoueur()-1){
                return getJoueur(0);
            }
            else{
                return getJoueur(getJoueurs().indexOf(joueur)+1);
            }
        }
        else{
            if(getJoueurs().indexOf(joueur) == 0){
                return getJoueur((getNombreJoueur()-1));
            }
            else{
                return getJoueur(getJoueurs().indexOf(joueur)-1);
            }
        }
    }

    public Joueur getJoueurSuivant(){
        return joueurSuivant;
    }

    //*********************************Méthodes*****************************************
    public void ajouter(Joueur joueur){
        if (joueur == null) {
            throw new IllegalArgumentException("Le joueur est null");
        }
        joueurs.add(joueur);
    }

    public void retirer(Joueur joueur){
        if (joueur == null) {
            throw new IllegalArgumentException("Le joueur est null");
        }
        joueurs.remove(joueur);
    }

    public void ajouterCartePioche(Carte carte){
        if (carte == null) {
            throw new IllegalArgumentException("carte null");
        }
        pioche.add(carte);
    }

    public void retirerCartePioche(int indice){
        if(indice < 0 || indice > getNombreCartePioche()-1){
            throw new IllegalArgumentException("indice non valide");
        }
        pioche.remove(indice);
    }

    public void retirerCarteTas(int indice){
        if(indice < 0 || indice > getNombreCarteTas()-1){
            throw new IllegalArgumentException("indice non valide");
        }
        tas.remove(indice);
    }

    /**
     * Renvoie la carte au sommet de la pioche.
     * @return Carte la carte du sommet de la pioche.
     */
    public Carte pioche(){
        if(getNombreCartePioche()==0){
            for (int i = 1; i < getNombreCarteTas(); i++) {
                ajouterCartePioche(getCarteTas(i));
                retirerCarteTas(i);
            }
        }
        Carte cartePioche = getCartePioche(0);
        retirerCartePioche(0);
        return cartePioche;
    }

    /**
     * Pose une carte au sommet du tas.
     * @param carte La carte a poser.
     */
    public void poser(Carte carte){
        tas.add(0,carte);
    }

    /**
     * Distribue @nombreCarteParJoueur, une par une a chaque joueur dans le sens horaire,
     * et pose la premiere carte du tas.
     * @param nombreCarteParJoueur Le nombre de carte a piocher.
     */
    public void distributionCarte(int nombreCarteParJoueur){
        for(int i=0; i<nombreCarteParJoueur; i++){
            for (Joueur joueur:joueurs
            ) {
                joueur.ajouter(pioche());
            }
        }
        tas.add(pioche());
        gestionPremiereCarteTas();
    }

    private void gestionPremiereCarteTas() {
        Carte carteTas = getCarteTas(0);
        if (carteTas.getClass() == CartePasser.class){
            getJoueurCourant().setTourDeJeu(false);
            getJoueurSuivant().setTourDeJeu(true);
            setJoueurSuivant(getJoueurSuivant(getJoueurSuivant()));
        }
        else if(carteTas.getClass() == CartePlus2.class){
            getJoueurCourant().setCarteAPiocher(2);
        }
        else if (carteTas.getClass()==CartePlus4.class){
            getJoueurCourant().setCarteAPiocher(4);
        }
        else if (carteTas.getClass() == CarteChangementDeSens.class){
            changementSens();
            setJoueurSuivant(getJoueurSuivant(getJoueurCourant()));
        }
    }


    /**
     * Initialise les regles d'une partie.
     */
    public void initialiserRegle(){
        try {
            regles = null;
            regles = new RegleCarteChangementDeSensSurCarteChangementDeSens(regles);
            regles = new RegleCarteChangementDeSensSurCarteCouleur(regles);
            regles = new RegleCarteChangementDeSensSurCartePasser(regles);
            regles = new RegleCarteChangementDeSensSurCartePlus2(regles);
            regles = new RegleCarteChangementDeSensSurCartePlus4(regles);
            regles = new RegleCarteChangementDeSensSurCarteSimple(regles);

            regles = new RegleCarteCouleurSurCarteChangementDeSens(regles);
            regles = new RegleCarteCouleurSurCarteCouleur(regles);
            regles = new RegleCarteCouleurSurCartePasser(regles);
            regles = new RegleCarteCouleurSurCartePlus2(regles);
            regles = new RegleCarteCouleurSurCartePlus4(regles);
            regles = new RegleCarteCouleurSurCarteSimple(regles);

            regles = new RegleCartePasserSurCarteChangementDeSens(regles);
            regles = new RegleCartePasserSurCarteCouleur(regles);
            regles = new RegleCartePasserSurCartePasser(regles);
            regles = new RegleCartePasserSurCartePlus2(regles);
            regles = new RegleCartePasserSurCartePlus4(regles);
            regles = new RegleCartePasserSurCarteSimple(regles);

            regles = new RegleCartePlus2SurCarteChangementDeSens(regles);
            regles = new RegleCartePlus2SurCarteCouleur(regles);
            regles = new RegleCartePlus2SurCartePasser(regles);
            regles = new RegleCartePlus2SurCartePlus2(regles);
            regles = new RegleCartePlus2SurCartePlus4(regles);
            regles = new RegleCartePlus2SurCarteSimple(regles);

            regles = new RegleCartePlus4SurCarteChangementDeSens(regles);
            regles = new RegleCartePlus4SurCarteCouleur(regles);
            regles = new RegleCartePlus4SurCartePasser(regles);
            regles = new RegleCartePlus4SurCartePlus2(regles);
            regles = new RegleCartePlus4SurCartePlus4(regles);
            regles = new RegleCartePlus4SurCarteSimple(regles);

            regles = new RegleCarteSimpleSurCarteChangementDeSens(regles);
            regles = new RegleCarteSimpleSurCarteCouleur(regles);
            regles = new RegleCarteSimpleSurCartePasser(regles);
            regles = new RegleCarteSimpleSurCartePlus2(regles);
            regles = new RegleCarteSimpleSurCartePlus4(regles);
            regles = new RegleCarteSimpleSurCarteSimple(regles);


        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }
        setRegles(regles);
    }

    /**
     * Parse un fichier contenant les cartes de la partie. Et creer la pioche de
     * celle-ci.
     * @param jeuDeCarte l'ensemble des cartes du jeu.
     */
    public void initialiser(JeuDeCarte jeuDeCarte){
        try {
            Parser premierParser = null;
            premierParser = new ParserCartePlus2(premierParser) ;
            premierParser = new ParserCarteSimple(premierParser) ;
            premierParser = new ParserCartePasser(premierParser) ;
            premierParser = new ParserCarteCouleur(premierParser) ;
            premierParser = new ParserCarteChangementDeSens(premierParser) ;
            premierParser = new ParserCartePlus4(premierParser) ;

            JeuDeCarte.lire(jeuDeCarte.getNomDuFichier(), premierParser,this);
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Reinitialise la partie.
     */
    public void reset(){
        pioche = new ArrayList<Carte>();
        tas = new ArrayList<Carte>();
        joueurs = new ArrayList<Joueur>();
        sensHoraire = true;
        regles = null;
    }
}
