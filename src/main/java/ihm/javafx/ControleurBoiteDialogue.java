package ihm.javafx;

import back.carte.Carte;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;

public class ControleurBoiteDialogue {
    @FXML
    private Button _boutonRouge;

    @FXML
    private Button _boutonVert;

    @FXML
    private Button _boutonBleu;

    @FXML
    private Button _boutonJaune;

    @FXML
    private Label _labelCouleur;

    @FXML
    private DialogPane _dialogPane;


    private String couleurChoisie;

    public ControleurBoiteDialogue() {
    }

    public void setCouleurChoisie(String couleurChoisie){
        this.couleurChoisie = couleurChoisie;
    }

    public String getCouleurChoisie() {
        return couleurChoisie;
    }

    @FXML
    public void initialize(){
        _dialogPane.lookupButton(ButtonType.OK).setDisable(true);
    }

    @FXML
    public void choixCouleur(ActionEvent actionEvent){
        Button bclic = (Button) (actionEvent.getSource());
        setCouleurChoisie(bclic.getText());
        _labelCouleur.setText(bclic.getText());
        _dialogPane.lookupButton(ButtonType.OK).setDisable(false);
    }
}
