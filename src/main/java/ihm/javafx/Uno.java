package ihm.javafx;

import back.carte.Carte;
import back.exception.CoupInterditException;
import back.exception.TourException;
import back.exception.UnoException;
import back.joueur.Joueur;
import back.parsage.JeuDeCarte;
import back.partie.Partie;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Classe principale de l'interface graphique.
 * Contient la scene du jeu avec les quatres joueurs (et leur jeux) et le sabot.
 */
public class Uno extends Application {

    private static final int H_CANVAS = 130;
    private static final int L_CANVAS = 400;
    private static final int L_CARTE = 80;
    private static final int ECART = 30;
    private Canvas canSabot;

    private ArrayList<String> listeCartes = new ArrayList<String>(); // Devrait disparaître en fonction des vos classes

    @Override
    public void start(Stage primaryStage) {
        try {
            BorderPane root = new BorderPane();

            Scene scene = new Scene(root);
            primaryStage.setScene(scene);

            Partie partie = Partie.getInstance();
            JeuDeCarte jeuDeCarte = new JeuDeCarte("jeux_test/JeuTest.csv");
            partie.initialiserRegle();
            partie.initialiser(jeuDeCarte);

            Joueur yann = new Joueur("Yann");
            Joueur camille = new Joueur("Camille");
            Joueur isabelle = new Joueur("Isabelle");
            Joueur charlotte = new Joueur("Charlotte");

            partie.ajouter(yann);
            partie.ajouter(camille);
            partie.ajouter(isabelle);
            partie.ajouter(charlotte);

            partie.setPremierJoueur(yann);
            partie.distributionCarte(3);


            VBox joueurNord = initJoueur(yann);
            root.setTop(joueurNord);

            VBox joueurOuest = initJoueur(camille);
            root.setRight(joueurOuest);

            VBox joueurSud = initJoueur(isabelle);
            root.setBottom(joueurSud);

            VBox joueurEst = initJoueur(charlotte);
            root.setLeft(joueurEst);


            root.setCenter(initSabot());

            primaryStage.show();

        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Initialise un joueur, selon son nom. Sa main et les boutons d'actions.
     * @param joueur Le joueur initialise.
     * @return VBox.
     */
    private VBox initJoueur(Joueur joueur) {
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);

        Label nomNord = initLabelNom(joueur);
        if(Partie.getInstance().getJoueurCourant().equals(joueur)){
            nomNord.setTextFill(Color.RED);
        }
        Canvas canMainNord = initMain(joueur);
        HBox unoNord = initBoutonUno(canMainNord,joueur);
        vBox.getChildren().addAll(nomNord,canMainNord,unoNord);
        return vBox;
    }


    /**
     * Initialise les boutons d'actions :
     * - Uno!.
     * - Pioche.
     * - Fin de tour.
     * @param canMain La main du joueur.
     * @param joueur Le joueur.
     * @return HBox.
     */
    private HBox initBoutonUno(Canvas canMain,Joueur joueur) {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        Button boutonUno = new Button("Uno !");

        boutonUno.setOnAction(select -> {
            System.out.println("Le joueur dit Uno !");
            try {
                joueur.direUno();
            } catch (UnoException e) {
                e.punirJoueur();
                updateMain(e.getJoueurPuni(),canMain);
            }
        });

        Button	boutonPioche = new Button("Pioche");
        boutonPioche.setOnAction(select -> {
            System.out.println("Le joueur pioche");
            boolean attaque = false;
            if(joueur.isAttaque()){
                attaque = true;
            }
            try {
                joueur.pioche();
            } catch (TourException e) {
                e.punirJoueur();
                updateMain(e.getJoueurPuni(),canMain);
            }
            updateMain(joueur,canMain);
            if(attaque){
                joueur.getLabel().setTextFill(Color.BLACK);
                Partie.getInstance().getJoueurCourant().getLabel().setTextFill(Color.RED);
            }
        });

        Button bouttonFinDeTour = new Button("Fin de tour");
        bouttonFinDeTour.setOnAction(select -> {
            System.out.println("le joueur finit son tour");
            joueur.getLabel().setTextFill(Color.BLACK);
            try {
                joueur.finDeTour();
            } catch (UnoException e) {
                e.punirJoueur();
                updateMain(e.getJoueurPuni(),canMain);
            } catch (TourException e) {
                e.punirJoueur();
                updateMain(e.getJoueurPuni(),canMain);
            }
            updateMain(joueur,canMain);
            Partie.getInstance().getJoueurCourant().getLabel().setTextFill(Color.RED);
        });
        hBox.getChildren().addAll(boutonUno,boutonPioche,bouttonFinDeTour);
        return hBox;
    }


    /**
     * Initialise le label d'un joueur.
     * @param joueur Le joueur.
     * @return Label.
     */
    private Label initLabelNom(Joueur joueur) {
        Label bidon = new Label("");
        bidon.setFont(new Font("Arial", 30));

        joueur.newLabel();
        joueur.getLabel().setText(joueur.getNom());
        joueur.getLabel().setFont(new Font("Arial", 30));

        return joueur.getLabel();
    }


    /**
     * Initialise le sabot (pioche et tas).
     * @return Canvas.
     */
    private Canvas initSabot() {
        canSabot = new Canvas();
        dessinerSabot();
        canSabot.setOnMouseClicked(clic -> {
            System.out.println("Pioche!");
        });
        return canSabot;
    }

    /**
     * Dessine sur la scene le sabot.
     */
    private void dessinerSabot() {
        Image sabot = new Image(String.valueOf(Uno.class.getResource("images/Sabot.png")));
        Image dos = new Image(String.valueOf(Uno.class.getResource("images/carte_dos.png")));
        canSabot.setWidth(sabot.getWidth());
        canSabot.setHeight(sabot.getHeight());

        Image imageCarte = new Image(String.valueOf(Uno.class.getResource(Partie.getInstance().getCarteTas().getImageURL())));

        canSabot.getGraphicsContext2D().drawImage(sabot,0,0);
        canSabot.getGraphicsContext2D().drawImage(imageCarte,25,20);
        canSabot.getGraphicsContext2D().drawImage(dos,124,20);
    }


    /**
     * Initialise la main d'un joueur.
     * @param joueur Le joueur.
     * @return Canvas.
     */
    private Canvas initMain(Joueur joueur) {
        Canvas canMain = new Canvas(L_CANVAS,H_CANVAS);
        dessinerMain(joueur.getJeu(), canMain);
        return poseCarte(joueur, canMain);
    }

    /**
     * Met a jour la main d'un joueur
     * @param joueur Le joueur.
     * @param canMain Le canvas contenant la main.
     * @return Canvas.
     */
    private Canvas updateMain(Joueur joueur, Canvas canMain) {
        dessinerMain(joueur.getJeu(), canMain);
        return poseCarte(joueur, canMain);
    }

    /**
     * Methode gerant la pose d'une carte dans l'interface graphique.
     * @param joueur Le joueur posant la carte.
     * @param canMain Le canvas du joueur posant la carte.
     * @return Canvas.
     */
    private Canvas poseCarte(Joueur joueur,Canvas canMain){
        canMain.setOnMouseClicked(clic -> {
            int x = (int) clic.getX();
            int nbCartes = joueur.getNombreCarte();
            int lMain = L_CARTE+((nbCartes-1)*ECART);
            int pad = (L_CANVAS-lMain) / 2;

            if (x>=pad && x<=pad+lMain) {
                int num = (int) ((x-pad) / ECART);
                num = Math.min(nbCartes-1, num);
                System.out.println("Le joueur a sélectionné la carte "+num);
                Carte cartePose =joueur.getCarteDuJeu(num);
                Alert dialogA = new Alert(Alert.AlertType.CONFIRMATION);
                dialogA.getButtonTypes().setAll(ButtonType.OK,ButtonType.CANCEL);
                dialogA.setTitle("Pose d'une carte");
                dialogA.setContentText("Voulez vous posez : "+cartePose);

                Optional<ButtonType> reponse = dialogA.showAndWait();
                if(reponse.get()==ButtonType.OK){
                    try {
                        joueur.poser(cartePose);
                    } catch (TourException e) {
                        e.punirJoueur();
                        updateMain(e.getJoueurPuni(),canMain);
                    } catch (CoupInterditException e) {
                        e.punirJoueur();
                        updateMain(e.getJoueurPuni(),canMain);
                        joueur.getLabel().setTextFill(Color.BLACK);
                        Partie.getInstance().getJoueurCourant().getLabel().setTextFill(Color.RED);
                    }
                    if(cartePose.isJoker()){
                        try {
                            selectCouleur(cartePose);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    updateMain(joueur,canMain);
                    dessinerSabot();
                }
                else{
                }
            }
        });
        return canMain;
    }

    /**
     * Methode qui gere la selection de la couleur lors de la pose d'une carte joker.
     * Ouvre une boite de dialogue pour ce choix.
     * @param cartePose La carte joker pose par le joueur.
     * @throws IOException .
     */
    private void selectCouleur( Carte cartePose) throws IOException {
        FXMLLoader loader = new FXMLLoader(Uno.class.getResource("ControleurBoiteDialogue.fxml"));
        ControleurBoiteDialogue controlleurBoiteDialogue = new ControleurBoiteDialogue();
        loader.setController(controlleurBoiteDialogue);
        DialogPane dialogPane = loader.load();

        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setTitle("Choisissez une couleur");
        dialog.setDialogPane(dialogPane);

        dialogPane.setMinWidth(200);

        Optional<ButtonType> clickButton = dialog.showAndWait();

        if(clickButton.get()==ButtonType.OK){
            cartePose.setCouleurJoker(controlleurBoiteDialogue.getCouleurChoisie());
        }
    }

    /**
     * Dessine la main d'un joueur.
     * @param liste Le jeu du joueur.
     * @param canvas Le canvas contenant le jeu du joueur.
     */
    private void dessinerMain(ArrayList<Carte> liste, Canvas canvas) {
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        int nbCartes = liste.size();
        int lMain = L_CARTE+((nbCartes-1)*ECART);
        int pad = (L_CANVAS-lMain) / 2;

        for (int i=0; i<nbCartes; i++) {
            Image carte = new Image((String.valueOf(Uno.class.getResource(liste.get(i).getImageURL()))));
            canvas.getGraphicsContext2D().drawImage(carte,pad+i*ECART,0);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}